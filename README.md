# SpacedRecall
> An app for learning information using spaced repetition method. Spaced Repetition Scheduling algorithm is based on Leitner Queue Network

## Table of contents
* [Technologies](#technologies)
	+ [Android](#android)
    + [Architecture](#architecture)
	+ [Domain](#domain)
    + [Testing](#testing)
* [Screenshots](#screenshots)

## Technologies

### Android
* [Data Binding](https://developer.android.com/topic/libraries/architecture/index.html)
A support library that allows you to bind UI components in your layouts to data sources in your app using a declarative format rather than programmatically.

### Architecture
* [Dagger2](https://github.com/google/dagger)
A compile-time framework for dependency injection.
* [Android Architecture Components](https://developer.android.com/topic/libraries/architecture)
Android architecture components are a collection of libraries that help you design robust, testable, and maintainable apps.

### Domain
* [Room](https://developer.android.com/topic/libraries/architecture/room)
The Room persistence library provides an abstraction layer over SQLite to allow for more robust database access while harnessing the full power of SQLite.
* [Retrofit](https://square.github.io/retrofit/)
Retrofit is a REST Client for Java and Android.
* [RxJava2](https://github.com/ReactiveX/RxJava)
RxJava is a Java VM implementation of Reactive Extensions: a library for composing asynchronous and event-based programs by using observable sequences.

### Testing
* [JUnit 4](https://junit.org/junit4/)
A simple framework to write repeatable tests. It is an instance of the xUnit architecture for unit testing frameworks.
* [Mockito](https://site.mockito.org/)
A mocking framework for unit tests written in Java
* [Espresso](https://developer.android.com/training/testing/espresso)
A UI testing framework for Android
* [MockWebServer](https://github.com/square/okhttp/tree/master/mockwebserver)
A scriptable web server for testing HTTP clients. It allows to mock server during instrumentation tests.

## Screenshots
![App screenshots](./images/spaced_recall_screnshots_small.png)


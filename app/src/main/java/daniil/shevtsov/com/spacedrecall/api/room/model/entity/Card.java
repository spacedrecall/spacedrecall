package daniil.shevtsov.com.spacedrecall.api.room.model.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "cards_table",
        foreignKeys = @ForeignKey(entity = Deck.class,
                parentColumns = "deck_id",
                childColumns = "deck_id",
                onDelete = ForeignKey.CASCADE))
public class Card {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "card_id")
    @NonNull
    private Long mCardId;

    @ColumnInfo(name = "question")
    private final String mQuestion;

    @ColumnInfo(name = "answer")
    private final String mAnswer;

    @ColumnInfo(name = "last_repetition_date")
    private Long mLastRepetitionDate;

    @ColumnInfo(name = "first_added_timestamp")
    private final Long mFirstAddedTimestamp;

    @ColumnInfo(name = "deck_id")
    private Long mDeckId;

    @Ignore
    public Card(@NonNull Long cardId, String question, String answer, Long lastRepetitionDate,
                Long firstAddedTimestamp, Long deckId) {
        mCardId = cardId;
        mQuestion = question;
        mAnswer = answer;
        mLastRepetitionDate = lastRepetitionDate;
        mFirstAddedTimestamp = firstAddedTimestamp;
        mDeckId = deckId;
    }

    public Card(String question, String answer, Long lastRepetitionDate,
                Long deckId, Long firstAddedTimestamp) {
        this(null, question, answer, lastRepetitionDate, firstAddedTimestamp, deckId);
    }

    @NonNull
    public Long getCardId() {
        return mCardId;
    }

    public void setCardId(@NonNull Long cardId) {
        mCardId = cardId;
    }

    public String getQuestion() {
        return mQuestion;
    }

    public String getAnswer() {
        return mAnswer;
    }

    public Long getLastRepetitionDate() {
        return mLastRepetitionDate;
    }

    public void setLastRepetitionDate(@NonNull Long lastRepetitionDate) {
        mLastRepetitionDate = lastRepetitionDate;
    }

    public Long getFirstAddedTimestamp() {
        return mFirstAddedTimestamp;
    }

    public Long getDeckId() {
        return mDeckId;
    }

    public void setDeckId(Long deckId) {
        mDeckId = deckId;
    }
}

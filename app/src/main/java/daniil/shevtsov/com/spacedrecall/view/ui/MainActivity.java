package daniil.shevtsov.com.spacedrecall.view.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import javax.inject.Inject;

import daniil.shevtsov.com.spacedrecall.R;
import daniil.shevtsov.com.spacedrecall.android.MyApplication;
import daniil.shevtsov.com.spacedrecall.databinding.ActivityMainBinding;
import daniil.shevtsov.com.spacedrecall.util.Logger;
import daniil.shevtsov.com.spacedrecall.viewmodel.MainViewModel;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    @Inject
    Logger mLogger;
    @Inject
    ViewModelProvider.Factory mMainViewModelFactory;

    private MainViewModel mMainViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MyApplication myApplication = (MyApplication) getApplication();

        if (myApplication == null) {
            Log.e(TAG, "Application cannot be cast to " + MyApplication.class.getName());
            return;
        }

        myApplication.getAppComponent().inject(this);

        mMainViewModel = ViewModelProviders.of(this, mMainViewModelFactory)
                .get(MainViewModel.class);

        initDataBinding();

        mMainViewModel.getNavigateToAddCardsEvent().observe(this, aVoid -> {
            Intent intent = new Intent(MainActivity.this, AddCardsActivity.class);
            startActivity(intent);
        });

        mMainViewModel.getNavigateToStudyEvent().observe(this, aVoid -> {
            Intent intent = new Intent(MainActivity.this, StudyCardsActivity.class);
            startActivity(intent);
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        mMainViewModel.initialiseDecks();
    }

    private void initDataBinding() {
        ActivityMainBinding activityMainBinding = DataBindingUtil
                .setContentView(this, R.layout.activity_main);

        activityMainBinding.setViewModel(mMainViewModel);
    }
}

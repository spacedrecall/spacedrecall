package daniil.shevtsov.com.spacedrecall.api.retrofit;

import javax.inject.Inject;

public class ApiClient {
    private final RestApiInterface mRestApiInterface;

    @Inject
    public ApiClient(RestApiInterface restApiInterface) {
        mRestApiInterface = restApiInterface;
    }
}

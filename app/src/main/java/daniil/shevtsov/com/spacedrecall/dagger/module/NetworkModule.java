package daniil.shevtsov.com.spacedrecall.dagger.module;

import dagger.Module;
import dagger.Provides;
import daniil.shevtsov.com.spacedrecall.api.retrofit.ApiClient;
import daniil.shevtsov.com.spacedrecall.api.retrofit.RestApiInterface;
import daniil.shevtsov.com.spacedrecall.dagger.scope.ApplicationScope;

@Module(includes = RetrofitModule.class)
public class NetworkModule {
    @Provides
    @ApplicationScope
    ApiClient provideApiClient(RestApiInterface restApiInterface) {
        return new ApiClient(restApiInterface);
    }
}

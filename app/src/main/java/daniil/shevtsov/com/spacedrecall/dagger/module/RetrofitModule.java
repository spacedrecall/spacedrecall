package daniil.shevtsov.com.spacedrecall.dagger.module;

import dagger.Module;
import dagger.Provides;
import daniil.shevtsov.com.spacedrecall.dagger.scope.ApplicationScope;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(includes = CommonRetrofitModule.class)
public class RetrofitModule {
    private static final String BASE_URL = "http://10.0.2.2:8080/SpacedRecallServer_Web_exploded/rest/";

    @Provides
    @ApplicationScope
    Retrofit provideRetrofit(OkHttpClient okHttpClient, GsonConverterFactory gsonConverterFactory) {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build();
    }
}

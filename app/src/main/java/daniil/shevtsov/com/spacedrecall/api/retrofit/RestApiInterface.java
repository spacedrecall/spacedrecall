package daniil.shevtsov.com.spacedrecall.api.retrofit;

import java.util.List;

import daniil.shevtsov.com.spacedrecall.api.retrofit.model.OptimizationProblemResult;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RestApiInterface {
    @GET("or-tools/calculate-optimal-service-rates")
    Single<OptimizationProblemResult> calculateOptimalServiceRates(
            @Query("arrival-rate")double arrivalRate,
            @Query("injection-rates") List<Double> injectionRates,
            @Query("review-frequency-budget") double reviewFrequencyBudget);
}

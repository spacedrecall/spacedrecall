package daniil.shevtsov.com.spacedrecall.view.ui;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import javax.inject.Inject;

import daniil.shevtsov.com.spacedrecall.R;
import daniil.shevtsov.com.spacedrecall.android.MyApplication;
import daniil.shevtsov.com.spacedrecall.databinding.ActivityAddCardsBinding;
import daniil.shevtsov.com.spacedrecall.viewmodel.AddCardsViewModel;

public class AddCardsActivity extends AppCompatActivity {
    private static final String TAG = AddCardsActivity.class.getSimpleName();

    private AddCardsViewModel mAddCardsViewModel;

    @Inject
    ViewModelProvider.Factory mAddCardsViewModelFactory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_cards);

        MyApplication myApplication = (MyApplication) getApplication();

        if(myApplication == null) {
            Log.e(TAG, "Application cannot be cast to " + MyApplication.class.getName());
            return;
        }

        myApplication.getAppComponent().inject(this);

        mAddCardsViewModel = ViewModelProviders.of(this, mAddCardsViewModelFactory)
                .get(AddCardsViewModel.class);

        initDataBinding();

        mAddCardsViewModel.getCardInsertedEvent().observe(this, aVoid -> {
            String insertMessage = getResources().getString(R.string.card_inserted_message);
            Toast.makeText(AddCardsActivity.this, insertMessage, Toast.LENGTH_SHORT).show();
        });
    }

    private void initDataBinding() {
        ActivityAddCardsBinding activityAddCardsBinding = DataBindingUtil
                .setContentView(this, R.layout.activity_add_cards);
        activityAddCardsBinding.setViewModel(mAddCardsViewModel);
        activityAddCardsBinding.setLifecycleOwner(this);
    }
}
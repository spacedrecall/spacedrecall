package daniil.shevtsov.com.spacedrecall.api.room.model.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import daniil.shevtsov.com.spacedrecall.api.room.CardsDatabase;
import daniil.shevtsov.com.spacedrecall.api.room.model.entity.SessionLog;
import io.reactivex.Single;

@Dao
public interface SessionLogDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Single<Long> insert(SessionLog sessionLog);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Single<List<Long>> insert(List<SessionLog> sessionLogs);

    @Query("SELECT * FROM " + CardsDatabase.SESSION_LOGS_TABLE_NAME + " WHERE session_log_id = :sessionLogId")
    Single<SessionLog> selectSessionLogWithId(Long sessionLogId);

    @Query("SELECT * FROM " + CardsDatabase.SESSION_LOGS_TABLE_NAME + " ORDER BY session_start_timestamp ASC")
    Single<List<SessionLog>> selectAllSessionLogs();

    @Query("DELETE FROM " + CardsDatabase.SESSION_LOGS_TABLE_NAME)
    void deleteAllSessionLogs();
}

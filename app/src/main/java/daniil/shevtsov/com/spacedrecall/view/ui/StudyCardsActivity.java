package daniil.shevtsov.com.spacedrecall.view.ui;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import javax.inject.Inject;

import daniil.shevtsov.com.spacedrecall.R;
import daniil.shevtsov.com.spacedrecall.android.MyApplication;
import daniil.shevtsov.com.spacedrecall.databinding.ActivityStudyCardsBinding;
import daniil.shevtsov.com.spacedrecall.viewmodel.StudyCardsViewModel;

public class StudyCardsActivity extends AppCompatActivity {
    private static final String TAG = StudyCardsActivity.class.getSimpleName();

    @Inject
    ViewModelProvider.Factory mStudyCardsViewModelFactory;

    private StudyCardsViewModel mStudyCardsViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_study_cards);

        MyApplication myApplication = (MyApplication) getApplication();

        if(myApplication == null) {
            Log.e(TAG, "Application cannot be cast to " + MyApplication.class.getName());
            return;
        }

        myApplication.getAppComponent().inject(this);


        mStudyCardsViewModel = ViewModelProviders.of(this, mStudyCardsViewModelFactory)
                .get(StudyCardsViewModel.class);

        initDataBinding();

        subscribeToObservables();

        mStudyCardsViewModel.onShowNextCard();
    }

    private void subscribeToObservables() {
        mStudyCardsViewModel.getShowNextCardEvent().observe(this, aVoid -> {
            mStudyCardsViewModel.onShowNextCard();
        });

        mStudyCardsViewModel.getCardsFinishedEvent().observe(this, aVoid -> {
            finish();
        });

        mStudyCardsViewModel.getShowErrorEvent().observe(this, errorMessageId -> {
            String errorMessage = getString(errorMessageId);
            Toast.makeText(StudyCardsActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
        });
    }

    private void initDataBinding() {
        ActivityStudyCardsBinding activityStudyCardsBinding = DataBindingUtil
                .setContentView(this, R.layout.activity_study_cards);
        activityStudyCardsBinding.setViewModel(mStudyCardsViewModel);
        activityStudyCardsBinding.setLifecycleOwner(this);
    }
}

package daniil.shevtsov.com.spacedrecall.dagger.module;

import dagger.Module;
import dagger.Provides;
import daniil.shevtsov.com.spacedrecall.api.room.CardsDatabase;
import daniil.shevtsov.com.spacedrecall.api.room.model.dao.CardDao;
import daniil.shevtsov.com.spacedrecall.api.room.model.dao.DeckDao;
import daniil.shevtsov.com.spacedrecall.api.room.model.dao.ReviewLogDao;
import daniil.shevtsov.com.spacedrecall.api.room.model.dao.SessionLogDao;
import daniil.shevtsov.com.spacedrecall.dagger.scope.ApplicationScope;
import daniil.shevtsov.com.spacedrecall.service.model.datetime.Clock;
import daniil.shevtsov.com.spacedrecall.service.model.datetime.DateWorker;
import daniil.shevtsov.com.spacedrecall.service.repository.database.ReviewLogsRepository;
import daniil.shevtsov.com.spacedrecall.service.repository.database.SessionLogsRepository;

@Module
public class CommonRoomModule {
    @Provides
    @ApplicationScope
    CardDao provideCardDao(CardsDatabase cardsDatabase) {
        return cardsDatabase.cardDao();
    }

    @Provides
    @ApplicationScope
    DeckDao provideDeckDao(CardsDatabase cardsDatabase) {
        return cardsDatabase.deckDao();
    }

    @Provides
    @ApplicationScope
    SessionLogDao provideSessionLogDao(CardsDatabase cardsDatabase) {
        return cardsDatabase.sessionLogDao();
    }

    @Provides
    @ApplicationScope
    ReviewLogDao provideReviewLogDao(CardsDatabase cardsDatabase) {
        return cardsDatabase.reviewLogDao();
    }

    @Provides
    @ApplicationScope
    ReviewLogsRepository provideReviewLogsRepository(ReviewLogDao reviewLogDao) {
        return new ReviewLogsRepository(reviewLogDao);
    }

    @Provides
    @ApplicationScope
    SessionLogsRepository provideSessionLogsRepository(SessionLogDao sessionLogDao) {
        return new SessionLogsRepository(sessionLogDao);
    }

    @Provides
    Clock provideClock() {
        return new Clock();
    }

    @Provides
    DateWorker provideDateWorker(Clock clock) {
        return new DateWorker(clock);
    }
}

package daniil.shevtsov.com.spacedrecall.android;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;

import javax.inject.Inject;

import daniil.shevtsov.com.spacedrecall.service.model.statistics.SessionLogger;

public class SessionTracker implements LifecycleObserver {
    private final SessionLogger mSessionLogger;


    @Inject
    public SessionTracker(SessionLogger sessionLogger) {
        mSessionLogger = sessionLogger;
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onLifecycleStart() {
        mSessionLogger.onSessionStarted();
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onLifecycleStop() {
        mSessionLogger.onSessionFinished();
    }
}

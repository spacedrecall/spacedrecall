package daniil.shevtsov.com.spacedrecall.dagger.module;

import dagger.Module;
import dagger.Provides;
import daniil.shevtsov.com.spacedrecall.api.retrofit.RestApiInterface;
import daniil.shevtsov.com.spacedrecall.dagger.scope.ApplicationScope;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class CommonRetrofitModule {

    @Provides
    @ApplicationScope
    HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        return httpLoggingInterceptor;
    }

    @Provides
    @ApplicationScope
    OkHttpClient provideOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor) {
        return new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .build();
    }

    @Provides
    @ApplicationScope
    GsonConverterFactory provideGsonConverterFactory() {
        return GsonConverterFactory.create();
    }

    @Provides
    @ApplicationScope
    RestApiInterface provideRestApiInterface(Retrofit retrofit) {
        return retrofit.create(RestApiInterface.class);
    }
}

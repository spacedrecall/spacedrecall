package daniil.shevtsov.com.spacedrecall.dagger.component;

import dagger.Component;
import daniil.shevtsov.com.spacedrecall.dagger.module.AppModule;
import daniil.shevtsov.com.spacedrecall.dagger.module.CommonModule;
import daniil.shevtsov.com.spacedrecall.dagger.module.NetworkModule;
import daniil.shevtsov.com.spacedrecall.dagger.module.RoomModule;
import daniil.shevtsov.com.spacedrecall.dagger.scope.ApplicationScope;

@ApplicationScope
@Component(modules = {AppModule.class, CommonModule.class, RoomModule.class, NetworkModule.class})
public interface ProductionAppComponent extends AppComponent {
}

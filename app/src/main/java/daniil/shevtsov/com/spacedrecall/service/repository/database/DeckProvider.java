package daniil.shevtsov.com.spacedrecall.service.repository.database;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import daniil.shevtsov.com.spacedrecall.api.room.model.dao.DeckDao;
import daniil.shevtsov.com.spacedrecall.api.room.model.entity.Deck;
import daniil.shevtsov.com.spacedrecall.util.Logger;
import io.reactivex.Completable;
import io.reactivex.Single;

public class DeckProvider {
    private static final String TAG = DeckProvider.class.getSimpleName();

    private final DeckDao mDeckDao;

    private final Logger mLogger;

    @Inject
    public DeckProvider(DeckDao deckDao, Logger logger) {
        mDeckDao = deckDao;
        mLogger = logger;
    }

    public Completable createDecks(int numberOfDecks) {
        mLogger.d(TAG, "create " + numberOfDecks + " of decks");

        List<Deck> decks = new ArrayList<>();
        for (int i = 0; i < numberOfDecks; ++i) {
            //TODO: use some predefined values
            Deck deck = new Deck(i+1, 0L, 0L, 0L);
            decks.add(deck);
        }

        return mDeckDao.insert(decks).toCompletable();
    }

    public Single<Deck> getEarliestDeck() {
        return mDeckDao.selectAllDecks()
                .flatMap(decks -> {
                    Deck earliestDeck = decks.get(0);

                    for (int i = 1; i < decks.size(); ++i) {
                        Deck deck = decks.get(i);
                        long leftTime = earliestDeck.getNextRepetitionTimestamp();
                        long rightTme = deck.getNextRepetitionTimestamp();

                        if (rightTme < leftTime) {
                            earliestDeck = deck;
                        }
                    }

                    return Single.just(earliestDeck);
                });
    }

    public Single<Long> getFirstDeckId() {
        return mDeckDao.selectFirstDeck().map(Deck::getDeckId);
    }

    public Single<Deck> getDeckForId(long deckId) {
        return mDeckDao.selectDeckWithId(deckId);
    }

    public Single<Boolean> checkIfDecksExist() {
        return mDeckDao.selectDecksCount().map(count -> count != 0);
    }

    public Completable updateRepetitionIntervals(List<Long> repetitionIntervals) {
        //TODO: Figure out how to do this correctly with rxjava2
        // TODO: Replace horrible deckNumber hack with something sensible
        return mDeckDao.selectAllDecks()
                .flattenAsObservable(decks -> decks)
                .flatMapCompletable(deck -> {
                    mLogger.d(TAG, "repetitionSize: " + repetitionIntervals.size() + " deckNumber: " + deck.getNumber());
                    return mDeckDao.updateRepetitionInterval(deck.getDeckId(), repetitionIntervals.get(deck.getNumber() - 1));
                });
    }

    public Single<Deck> getDeckForNumber(int nextDeckNumber) {
        return mDeckDao.selectDeckWithNumber(nextDeckNumber);
    }

    public Single<List<Deck>> getAllDecks() {
        return mDeckDao.selectAllDecks();
    }
}

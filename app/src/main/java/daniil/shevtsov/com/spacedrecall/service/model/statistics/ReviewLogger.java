package daniil.shevtsov.com.spacedrecall.service.model.statistics;

import javax.inject.Inject;

import daniil.shevtsov.com.spacedrecall.api.room.model.entity.ReviewLog;
import daniil.shevtsov.com.spacedrecall.service.repository.CurrentReviewLogsStorage;
import daniil.shevtsov.com.spacedrecall.service.repository.TimestampProvider;
import daniil.shevtsov.com.spacedrecall.service.repository.database.ReviewLogsRepository;
import daniil.shevtsov.com.spacedrecall.util.Logger;
import io.reactivex.Completable;

public class ReviewLogger {
    private static final String TAG = ReviewLogger.class.getSimpleName();

    private final TimestampProvider mTimestampProvider;
    private final ReviewLogsRepository mReviewLogsRepository;
    private final CurrentReviewLogsStorage mCurrentReviewLogsStorage;
    private final Logger mLogger;

    @Inject
    public ReviewLogger(TimestampProvider timestampProvider,
                        ReviewLogsRepository reviewLogsRepository,
                        CurrentReviewLogsStorage currentReviewLogsStorage, Logger logger) {
        mTimestampProvider = timestampProvider;
        mReviewLogsRepository = reviewLogsRepository;
        mCurrentReviewLogsStorage = currentReviewLogsStorage;
        mLogger = logger;
    }

    public void onCardReviewed(Long newDeckId) {
        long currentTimestamp = mTimestampProvider.getCurrentTimestamp();
        ReviewLog reviewLog = new ReviewLog(null, null,
                newDeckId, currentTimestamp);

        mCurrentReviewLogsStorage.addReviewLog(reviewLog);
    }

    public Completable onSessionFinished(Long currentSessionId) {
        mCurrentReviewLogsStorage.updateSessionId(currentSessionId);

        return mReviewLogsRepository.insertReviewLogs(mCurrentReviewLogsStorage.getReviewLogs());
    }
}

package daniil.shevtsov.com.spacedrecall.dagger.module;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import daniil.shevtsov.com.spacedrecall.dagger.scope.ApplicationScope;
import daniil.shevtsov.com.spacedrecall.util.Logger;

@Module
public
class AppModule {
    private final Context mContext;

    public AppModule(Context context) {
        mContext = context;
    }


    @Provides
    @ApplicationScope
    Context provideContext() {
        return mContext;
    }

    @Provides
    Logger provideLogger() {
        return new Logger();
    }
}

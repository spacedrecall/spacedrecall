package daniil.shevtsov.com.spacedrecall.service.model.schedule;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import daniil.shevtsov.com.spacedrecall.service.model.statistics.StatisticsCalculator;
import daniil.shevtsov.com.spacedrecall.service.repository.OptimalServiceRatesSolver;
import daniil.shevtsov.com.spacedrecall.service.repository.database.DeckProvider;
import daniil.shevtsov.com.spacedrecall.util.Logger;
import io.reactivex.Completable;
import io.reactivex.exceptions.Exceptions;

public class ReviewScheduleCalculator {
    private static final String TAG = ReviewScheduleCalculator.class.getSimpleName();

    private final OptimalServiceRatesSolver mOptimalServiceRatesSolver;
    private final StatisticsCalculator mStatisticsCalculator;
    private final DeckProvider mDeckProvider;
    private final Logger mLogger;

    private static final List<Double> DEFAULT_SERVICE_RATES = Arrays.asList(5.0, 4.0, 3.0, 2.0, 1.0);

    @Inject
    public ReviewScheduleCalculator(OptimalServiceRatesSolver optimalServiceRatesSolver,
                                    StatisticsCalculator statisticsCalculator, DeckProvider deckProvider, Logger logger) {

        mOptimalServiceRatesSolver = optimalServiceRatesSolver;
        mStatisticsCalculator = statisticsCalculator;
        mDeckProvider = deckProvider;
        mLogger = logger;
    }

    public Completable updateServiceRates() {
        return mStatisticsCalculator.calculateOptimizationInputData()
                .doOnError(Exceptions::propagate)
                .flatMapCompletable(optimizationInputData ->
                        mOptimalServiceRatesSolver.calculateOptimalServiceRates(optimizationInputData)
                                .flatMapCompletable(optimalServiceRates -> {
                                    List<Long> repetitionIntervals = mStatisticsCalculator.calculateRepetitionIntervals(optimalServiceRates);
                                    return mDeckProvider.updateRepetitionIntervals(repetitionIntervals);
                                }));
    }
}

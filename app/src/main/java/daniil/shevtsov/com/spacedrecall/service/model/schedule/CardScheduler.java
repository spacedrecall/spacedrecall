package daniil.shevtsov.com.spacedrecall.service.model.schedule;

import javax.inject.Inject;

import daniil.shevtsov.com.spacedrecall.api.room.model.entity.Card;
import daniil.shevtsov.com.spacedrecall.service.repository.TimestampProvider;
import daniil.shevtsov.com.spacedrecall.service.repository.database.CardProvider;
import daniil.shevtsov.com.spacedrecall.service.repository.database.DeckProvider;
import daniil.shevtsov.com.spacedrecall.util.Logger;
import io.reactivex.Completable;
import io.reactivex.Single;

public class CardScheduler {
    private static final String TAG = CardScheduler.class.getSimpleName();

    private final DeckProvider mDeckProvider;
    private final CardProvider mCardProvider;
    private final TimestampProvider mTimestampProvider;
    private final NextDeckCalculator mNextDeckCalculator;
    private final ReviewScheduleCalculator mReviewScheduleCalculator;
    private final Logger mLogger;

    @Inject
    public CardScheduler(DeckProvider deckProvider, CardProvider cardProvider,
                         TimestampProvider timestampProvider, NextDeckCalculator nextDeckCalculator,
                         ReviewScheduleCalculator reviewScheduleCalculator, Logger logger) {
        mDeckProvider = deckProvider;

        mCardProvider = cardProvider;
        mTimestampProvider = timestampProvider;
        mNextDeckCalculator = nextDeckCalculator;
        mReviewScheduleCalculator = reviewScheduleCalculator;
        mLogger = logger;
    }

    public Completable recalculateServiceRates(Card card) {
                return mCardProvider.updateLastRepetitionTime(card.getCardId(), mTimestampProvider.getCurrentTimestamp())
                .andThen(mReviewScheduleCalculator.updateServiceRates());
    }

    public Single<Long> moveCardToAnotherDeck(Card card, boolean isCardRecalled) {
        return mDeckProvider.getDeckForId(card.getDeckId())
                .map(deck -> mNextDeckCalculator.calculateDeckNumber(deck.getNumber(), isCardRecalled))
                .flatMap(mDeckProvider::getDeckForNumber)
                .flatMap(newDeck -> mCardProvider.moveCardToAnotherDeck(card, newDeck.getDeckId()).andThen(Single.just(newDeck.getDeckId())));
    }

    public Single<Card> getNextCardToReview() {
        return mDeckProvider.getEarliestDeck()
                .flatMap(deck -> mCardProvider.getEarliestCardFromDeck(deck.getDeckId()));
    }

    public Single<Boolean> checkIfMoreCardsToRepeatAvailable() {
        return mDeckProvider.getEarliestDeck()
                .flatMap(deck -> mCardProvider.countCardsAvailableForRepetition(deck.getDeckId()))
                .map(availableCardsCount -> (availableCardsCount != 0));
    }
}

package daniil.shevtsov.com.spacedrecall.service.repository.database;

import java.util.List;

import javax.inject.Inject;

import daniil.shevtsov.com.spacedrecall.api.room.model.dao.SessionLogDao;
import daniil.shevtsov.com.spacedrecall.api.room.model.entity.SessionLog;
import io.reactivex.Single;

public class SessionLogsRepository {
    private final SessionLogDao mSessionLogDao;

    @Inject
    public SessionLogsRepository(SessionLogDao sessionLogDao) {
        mSessionLogDao = sessionLogDao;
    }

    public Single<Long> addNewSessionLog(SessionLog sessionLog) {
        return mSessionLogDao.insert(sessionLog);
    }

    public Single<List<SessionLog>> getSessionLogs() {
        return mSessionLogDao.selectAllSessionLogs();
    }
}

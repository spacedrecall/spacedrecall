package daniil.shevtsov.com.spacedrecall.api.room.model.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import daniil.shevtsov.com.spacedrecall.api.room.CardsDatabase;

@Entity(tableName = CardsDatabase.SESSION_LOGS_TABLE_NAME)
public class SessionLog {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "session_log_id")
    @NonNull
    private Long mSessionLogId;

    @ColumnInfo(name = "session_start_timestamp")
    private Long mSessionStartTimestamp;

    @ColumnInfo(name = "session_end_timestamp")
    private Long mSessionEndTimestamp;

    @ColumnInfo(name = "added_cards_count")
    private Integer mAddedCardsCount;

    @ColumnInfo(name = "reviewed_cards_count")
    private Integer mReviewedCardsCount;

    @ColumnInfo(name = "memorized_cards_count")
    private Integer mMemorizedCardsCount;

    @ColumnInfo(name = "review_frequency_budget")
    private Double mReviewFrequencyBudget;

    @Ignore
    public SessionLog(@NonNull Long sessionLogId, Long sessionStartTimestamp, Long sessionEndTimestamp,
                      Integer addedCardsCount, Integer reviewedCardsCount, Integer memorizedCardsCount,
                      Double reviewFrequencyBudget) {
        mSessionLogId = sessionLogId;
        mSessionStartTimestamp = sessionStartTimestamp;
        mSessionEndTimestamp = sessionEndTimestamp;
        mAddedCardsCount = addedCardsCount;
        mReviewedCardsCount = reviewedCardsCount;
        mMemorizedCardsCount = memorizedCardsCount;
        mReviewFrequencyBudget = reviewFrequencyBudget;
    }

    public SessionLog(Long sessionStartTimestamp, Long sessionEndTimestamp,
                      Integer addedCardsCount, Integer reviewedCardsCount, Integer memorizedCardsCount,
                      Double reviewFrequencyBudget) {
        this(null, sessionStartTimestamp, sessionEndTimestamp, addedCardsCount, reviewedCardsCount,
                memorizedCardsCount, reviewFrequencyBudget);
    }

    @NonNull
    public Long getSessionLogId() {
        return mSessionLogId;
    }

    public Long getSessionStartTimestamp() {
        return mSessionStartTimestamp;
    }

    public Long getSessionEndTimestamp() {
        return mSessionEndTimestamp;
    }

    public Integer getAddedCardsCount() {
        return mAddedCardsCount;
    }

    public Integer getReviewedCardsCount() {
        return mReviewedCardsCount;
    }

    public Integer getMemorizedCardsCount() {
        return mMemorizedCardsCount;
    }

    public Double getReviewFrequencyBudget() {
        return mReviewFrequencyBudget;
    }

    public void setSessionLogId(@NonNull Long sessionLogId) {
        mSessionLogId = sessionLogId;
    }

    public void setSessionStartTimestamp(Long sessionStartTimestamp) {
        mSessionStartTimestamp = sessionStartTimestamp;
    }

    public void setSessionEndTimestamp(Long sessionEndTimestamp) {
        mSessionEndTimestamp = sessionEndTimestamp;
    }

    public void setAddedCardsCount(Integer addedCardsCount) {
        mAddedCardsCount = addedCardsCount;
    }

    public void setReviewedCardsCount(Integer reviewedCardsCount) {
        mReviewedCardsCount = reviewedCardsCount;
    }

    public void setMemorizedCardsCount(Integer memorizedCardsCount) {
        mMemorizedCardsCount = memorizedCardsCount;
    }

    public void setReviewFrequencyBudget(Double reviewFrequencyBudget) {
        mReviewFrequencyBudget = reviewFrequencyBudget;
    }
}

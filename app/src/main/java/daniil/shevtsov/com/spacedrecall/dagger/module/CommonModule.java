package daniil.shevtsov.com.spacedrecall.dagger.module;

import dagger.Module;

@Module(includes = {ViewModelModule.class, StatisticsModule.class, SchedulerModule.class})
public class CommonModule {
}

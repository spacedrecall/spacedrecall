package daniil.shevtsov.com.spacedrecall.api.room.model.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import daniil.shevtsov.com.spacedrecall.api.room.model.entity.ReviewLog;
import io.reactivex.Single;

@Dao
public interface ReviewLogDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Single<Long> insert(ReviewLog reviewLog);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Single<List<Long>> insert(List<ReviewLog> reviewLogs);

    @Query("SELECT * FROM review_logs_table WHERE review_log_id = :reviewLogId")
    Single<ReviewLog> selectReviewLogWithId(Long reviewLogId);

    @Query("SELECT * FROM review_logs_table ORDER BY timestamp ASC")
    Single<List<ReviewLog>> selectAllReviewLogs();

    @Query("SELECT COUNT(*) FROM review_logs_table WHERE session_log_id = :sessionLogId" +
            " AND new_deck_id = :newDeckId")
    Single<Integer> selectReviewCountForDeckWithSessionLogId(Long newDeckId, Long sessionLogId);
}

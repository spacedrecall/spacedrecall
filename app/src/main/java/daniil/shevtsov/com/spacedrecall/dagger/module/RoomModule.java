package daniil.shevtsov.com.spacedrecall.dagger.module;

import android.content.Context;

import androidx.room.Room;

import dagger.Module;
import dagger.Provides;
import daniil.shevtsov.com.spacedrecall.api.room.CardsDatabase;
import daniil.shevtsov.com.spacedrecall.dagger.scope.ApplicationScope;

@Module(includes = CommonRoomModule.class)
public class RoomModule {
    @Provides
    @ApplicationScope
    CardsDatabase provideCardsDatabase(Context context) {
        return Room.databaseBuilder(context,
                CardsDatabase.class, CardsDatabase.DATABASE_NAME)
                .addMigrations(CardsDatabase.MIGRATIONS)
                .build();
    }
}

package daniil.shevtsov.com.spacedrecall.api.room.model.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import daniil.shevtsov.com.spacedrecall.api.room.model.entity.Deck;
import io.reactivex.Completable;
import io.reactivex.Single;

@Dao
public interface DeckDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Single<Long> insert(Deck deck);

    @Insert
    Single<List<Long>> insert(List<Deck> decks);

    @Query("SELECT * FROM decks_table LIMIT 1")
    Single<Deck> selectOneDeck();

    @Query("SELECT * FROM decks_table ORDER BY number ASC")
    Single<List<Deck>> selectAllDecks();

    @Query("SELECT * FROM decks_table WHERE deck_id = :deckId")
    Single<Deck> selectDeckWithId(long deckId);

    @Query("SELECT * FROM decks_table WHERE number = :deckNumber")
    Single<Deck> selectDeckWithNumber(int deckNumber);

    @Query("SELECT * FROM decks_table ORDER BY number ASC LIMIT 1")
    Single<Deck> selectFirstDeck();

    @Query("SELECT COUNT(*) FROM decks_table")
    Single<Integer> selectDecksCount();

    @Query("SELECT * FROM decks_table where next_repetition_timestamp <= :currentTimestamp")
    Single<List<Deck>> selectDecksToRepeat(long currentTimestamp);

    @Query("UPDATE decks_table SET repetition_interval = :newRepetitionInterval WHERE deck_id = :deckId")
    Completable updateRepetitionInterval(Long deckId, Long newRepetitionInterval);

    @Query("DELETE FROM decks_table")
    void deleteAllDecks();

}

package daniil.shevtsov.com.spacedrecall.api.retrofit.model;


import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OptimizationProblemResult {
    @SerializedName("arrivalRate")
    private double mArrivalRate;

    @SerializedName("serviceRates")
    private List<Double> mServiceRates;

    public OptimizationProblemResult(double arrivalRate, List<Double> serviceRates) {
        mArrivalRate = arrivalRate;
        mServiceRates = serviceRates;
    }

    public double getArrivalRate() {
        return mArrivalRate;
    }

    public void setArrivalRate(double arrivalRate) {
        mArrivalRate = arrivalRate;
    }

    public List<Double> getServiceRates() {
        return mServiceRates;
    }

    public void setServiceRates(List<Double> serviceRates) {
        mServiceRates = serviceRates;
    }
}

package daniil.shevtsov.com.spacedrecall.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import javax.inject.Inject;
import javax.inject.Named;

import daniil.shevtsov.com.spacedrecall.api.room.model.entity.Card;
import daniil.shevtsov.com.spacedrecall.dagger.module.SchedulerModule;
import daniil.shevtsov.com.spacedrecall.service.model.statistics.SessionLogger;
import daniil.shevtsov.com.spacedrecall.service.repository.TimestampProvider;
import daniil.shevtsov.com.spacedrecall.service.repository.database.CardProvider;
import daniil.shevtsov.com.spacedrecall.service.repository.database.DeckProvider;
import daniil.shevtsov.com.spacedrecall.util.Logger;
import daniil.shevtsov.com.spacedrecall.util.SingleLiveEvent;
import io.reactivex.Scheduler;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class AddCardsViewModel extends ViewModel {
    private static final String TAG = AddCardsViewModel.class.getSimpleName();

    private final MutableLiveData<String> mQuestionTextObservable = new MutableLiveData<>();
    private final MutableLiveData<String> mAnswerTextObservable = new MutableLiveData<>();

    private final MutableLiveData<Boolean> mIsCardValidObservable = new MutableLiveData<>();
    private final CardProvider mCardProvider;
    private final DeckProvider mDeckProvider;
    private final SessionLogger mSessionLogger;
    private final Logger mLogger;
    private final TimestampProvider mTimestampProvider;

    private final Scheduler mBackgroundScheduler;
    private final Scheduler mMainScheduler;

    private final CompositeDisposable mCompositeDisposable;

    private final SingleLiveEvent<Void> mCardInsertedEvent = new SingleLiveEvent<>();

    @Inject
    public AddCardsViewModel(CardProvider cardProvider, DeckProvider deckProvider, SessionLogger sessionLogger, Logger logger,
                             TimestampProvider timestampProvider,
                             @Named(SchedulerModule.BACKGROUND_THREAD) Scheduler backgroundScheduler,
                             @Named(SchedulerModule.MAIN_THREAD) Scheduler mainScheduler) {
        mCardProvider = cardProvider;
        mDeckProvider = deckProvider;
        mSessionLogger = sessionLogger;
        mLogger = logger;
        mTimestampProvider = timestampProvider;
        mBackgroundScheduler = backgroundScheduler;
        mMainScheduler = mainScheduler;

        mCompositeDisposable = new CompositeDisposable();
    }

    @Override
    protected void onCleared() {
        super.onCleared();

        mCompositeDisposable.dispose();
    }

    public SingleLiveEvent<Void> getCardInsertedEvent() {
        return mCardInsertedEvent;
    }

    public void onAddCard() {
        mLogger.d(TAG, "onAddCard");
        String questionText = mQuestionTextObservable.getValue();
        String answerText = mAnswerTextObservable.getValue();

        if (questionText == null) {
            mLogger.e(TAG, "questionText is null");
            return;
        }

        if (answerText == null) {
            mLogger.e(TAG, "answerText is null");
            return;
        }

        Disposable disposable = mDeckProvider.getFirstDeckId()
                .subscribeOn(mBackgroundScheduler)
                .flatMapCompletable(firstDeckId -> {
                    Card newCard = new Card(questionText, answerText, 0L,
                            firstDeckId, mTimestampProvider.getCurrentTimestamp());
                    mLogger.d(TAG, "mCardProvider.addNewCard");

                    return mCardProvider.addNewCard(newCard);
                }).observeOn(mMainScheduler)
                .subscribe(() -> {
                            mSessionLogger.onCardAdded();
                            mCardInsertedEvent.call();
                        },
                        throwable -> mLogger.e(TAG, "onAddCards getFirstDeckId: " + throwable.getMessage()));

        mCompositeDisposable.add(disposable);
    }

    public MutableLiveData<String> getQuestionTextObservable() {
        return mQuestionTextObservable;
    }

    public MutableLiveData<String> getAnswerTextObservable() {
        return mAnswerTextObservable;
    }

    public LiveData<Boolean> getIsCardValidObservable() {
        return mIsCardValidObservable;
    }

    public void onQuestionTextChange(String questionText) {
        mQuestionTextObservable.setValue(questionText);

        boolean isCardValid = validateCard();
        mIsCardValidObservable.setValue(isCardValid);
    }

    public void onAnswerTextChange(String answerText) {
        mAnswerTextObservable.setValue(answerText);

        boolean isCardValid = validateCard();
        mIsCardValidObservable.setValue(isCardValid);
    }

    private boolean validateCard() {
        String questionText = mQuestionTextObservable.getValue();
        String answerText = mAnswerTextObservable.getValue();

        boolean isQuestionValid = questionText != null && !questionText.isEmpty();
        boolean isAnswerValid = answerText != null && !answerText.isEmpty();

        return isQuestionValid && isAnswerValid;
    }
}

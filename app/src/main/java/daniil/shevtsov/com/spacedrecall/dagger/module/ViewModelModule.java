package daniil.shevtsov.com.spacedrecall.dagger.module;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;
import daniil.shevtsov.com.spacedrecall.dagger.viewmodelinjection.DaggerViewModelFactory;
import daniil.shevtsov.com.spacedrecall.dagger.viewmodelinjection.ViewModelKey;
import daniil.shevtsov.com.spacedrecall.viewmodel.AddCardsViewModel;
import daniil.shevtsov.com.spacedrecall.viewmodel.MainViewModel;
import daniil.shevtsov.com.spacedrecall.viewmodel.StudyCardsViewModel;

@Module
public abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel.class)
    abstract ViewModel bindMainViewModel(MainViewModel mainViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(AddCardsViewModel.class)
    abstract ViewModel bindAddCardsViewModel(AddCardsViewModel addCardsViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(StudyCardsViewModel.class)
    abstract ViewModel bindStudyCardsViewModel(StudyCardsViewModel studyCardsViewModel);

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(DaggerViewModelFactory factory);
}

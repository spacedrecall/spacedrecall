package daniil.shevtsov.com.spacedrecall.android;

import android.app.Application;

import androidx.lifecycle.ProcessLifecycleOwner;

import javax.inject.Inject;

import daniil.shevtsov.com.spacedrecall.dagger.component.AppComponent;
import daniil.shevtsov.com.spacedrecall.dagger.component.DaggerProductionAppComponent;
import daniil.shevtsov.com.spacedrecall.dagger.module.AppModule;
import daniil.shevtsov.com.spacedrecall.dagger.module.CommonModule;
import daniil.shevtsov.com.spacedrecall.dagger.module.RoomModule;

public class MyApplication extends Application {
    private AppComponent mAppComponent;

    @Inject
    public SessionTracker mSessionTracker;

    @Override
    public void onCreate() {
        super.onCreate();

        mAppComponent = DaggerProductionAppComponent.builder()
                .commonModule(new CommonModule())
                .appModule(new AppModule(this))
                .roomModule(new RoomModule())
                .build();

        mAppComponent.inject(this);

        ProcessLifecycleOwner.get().getLifecycle().addObserver(mSessionTracker);
    }

    public  AppComponent getAppComponent() {
        return mAppComponent;
    }
}

package daniil.shevtsov.com.spacedrecall.service.repository;

import java.util.List;

import javax.inject.Inject;

import daniil.shevtsov.com.spacedrecall.api.retrofit.RestApiInterface;
import daniil.shevtsov.com.spacedrecall.api.retrofit.model.OptimizationProblemResult;
import daniil.shevtsov.com.spacedrecall.service.model.schedule.OptimizationInputData;
import daniil.shevtsov.com.spacedrecall.util.Logger;
import io.reactivex.Single;

public class OptimalServiceRatesSolver {
    private static final String TAG = OptimalServiceRatesSolver.class.getSimpleName();

    private final RestApiInterface mRestApiInterface;
    private final Logger mLogger;


    @Inject
    public OptimalServiceRatesSolver(RestApiInterface restApiInterface, Logger logger) {
        mRestApiInterface = restApiInterface;
        mLogger = logger;
    }

    public Single<List<Double>> calculateOptimalServiceRates(OptimizationInputData optimizationInputData) {
        return mRestApiInterface.calculateOptimalServiceRates(
                optimizationInputData.getArrivalRate(),
                optimizationInputData.getInjectionRates(),
                optimizationInputData.getReviewFrequencyBudget()
        ).map(OptimizationProblemResult::getServiceRates);
    }
}

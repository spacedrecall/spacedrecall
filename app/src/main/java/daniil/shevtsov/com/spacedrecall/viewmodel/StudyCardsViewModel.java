package daniil.shevtsov.com.spacedrecall.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.net.SocketTimeoutException;

import javax.inject.Inject;
import javax.inject.Named;

import daniil.shevtsov.com.spacedrecall.R;
import daniil.shevtsov.com.spacedrecall.api.room.model.entity.Card;
import daniil.shevtsov.com.spacedrecall.dagger.module.SchedulerModule;
import daniil.shevtsov.com.spacedrecall.service.model.schedule.CardScheduler;
import daniil.shevtsov.com.spacedrecall.service.model.statistics.ReviewLogger;
import daniil.shevtsov.com.spacedrecall.service.model.statistics.SessionLogger;
import daniil.shevtsov.com.spacedrecall.util.Logger;
import daniil.shevtsov.com.spacedrecall.util.SingleLiveEvent;
import io.reactivex.Scheduler;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import retrofit2.HttpException;

public class StudyCardsViewModel extends ViewModel {
    private static final String TAG = StudyCardsViewModel.class.getSimpleName();

    private static final int SOLUTION_NOT_FOUND_HTTP_ERROR_CODE = 452;
    private static final int NAN_INJECTION_RATES_HTTP_ERROR_CODE = 453;

    private final MutableLiveData<String> mQuestionTextObservable = new MutableLiveData<>();
    private final MutableLiveData<String> mAnswerTextObservable = new MutableLiveData<>();
    private final MutableLiveData<Boolean> mIsShowAnswerButtonVisibleLiveData = new MutableLiveData<>();
    private final MutableLiveData<Boolean> mIsRecallButtonsVisibleLiveData = new MutableLiveData<>();

    private final SingleLiveEvent<Void> mShowAnswerEvent = new SingleLiveEvent<>();
    private final SingleLiveEvent<Void> mShowNextCardEvent = new SingleLiveEvent<>();
    private final SingleLiveEvent<Void> mCardsFinishedEvent = new SingleLiveEvent<>();
    private final SingleLiveEvent<Integer> mShowErrorEvent = new SingleLiveEvent<>();

    private final CardScheduler mCardScheduler;
    private final Logger mLogger;
    private final SessionLogger mSessionLogger;
    private final ReviewLogger mReviewLogger;

    private final Scheduler mBackgroundScheduler;
    private final Scheduler mMainScheduler;

    private final CompositeDisposable mCompositeDisposable;

    private Card mCurrentCard;

    @Inject
    public StudyCardsViewModel(CardScheduler cardScheduler, Logger logger,
                               SessionLogger sessionLogger, ReviewLogger reviewLogger,
                               @Named(SchedulerModule.BACKGROUND_THREAD) Scheduler backgroundScheduler,
                               @Named(SchedulerModule.MAIN_THREAD) Scheduler mainScheduler) {
        mCardScheduler = cardScheduler;
        mLogger = logger;
        mSessionLogger = sessionLogger;
        mReviewLogger = reviewLogger;
        mBackgroundScheduler = backgroundScheduler;
        mMainScheduler = mainScheduler;

        mCompositeDisposable = new CompositeDisposable();

        setBottomPanelVisibility(true);
    }

    public LiveData<String> getQuestionTextObservable() {
        return mQuestionTextObservable;
    }

    public LiveData<String> getAnswerTextObservable() {
        return mAnswerTextObservable;
    }

    public LiveData<Boolean> getIsShowAnswerButtonVisibleLiveData() {
        return mIsShowAnswerButtonVisibleLiveData;
    }

    public LiveData<Boolean> getIsRecallButtonsVisibleLiveData() {
        return mIsRecallButtonsVisibleLiveData;
    }

    public SingleLiveEvent<Void> getShowAnswerEvent() {
        return mShowAnswerEvent;
    }

    public SingleLiveEvent<Void> getShowNextCardEvent() {
        return mShowNextCardEvent;
    }

    public SingleLiveEvent<Void> getCardsFinishedEvent() {
        return mCardsFinishedEvent;
    }

    public SingleLiveEvent<Integer> getShowErrorEvent() {
        return mShowErrorEvent;
    }

    @Override
    protected void onCleared() {
        mCompositeDisposable.dispose();
    }

    public void onShowAnswer() {
        if (mCurrentCard == null) {
            mLogger.e(TAG, "onShowAnswer: current card is null");

            return;
        }

        mAnswerTextObservable.setValue(mCurrentCard.getAnswer());

        setBottomPanelVisibility(false);

        mShowAnswerEvent.call();
    }

    private void setBottomPanelVisibility(boolean isShowAnswerButtonVisible) {
        mIsShowAnswerButtonVisibleLiveData.postValue(isShowAnswerButtonVisible);
        mIsRecallButtonsVisibleLiveData.postValue(!isShowAnswerButtonVisible);
    }

    public void onCardReviewed(boolean isCardRecalled) {
        Disposable disposable = mCardScheduler.recalculateServiceRates(mCurrentCard)
                .andThen(mCardScheduler.moveCardToAnotherDeck(mCurrentCard, isCardRecalled))
                .subscribeOn(mBackgroundScheduler)
                .observeOn(mMainScheduler)
                .subscribe(newDeckId -> {
                    mSessionLogger.onCardReviewed();
                    mReviewLogger.onCardReviewed(newDeckId);

                    setBottomPanelVisibility(true);

                    mShowNextCardEvent.call();
                }, throwable -> {
                    mLogger.e(TAG, "onCardReviewed: " + throwable.getMessage());

                    if (throwable instanceof SocketTimeoutException) {
                        mShowErrorEvent.setValue(R.string.server_timeout_error_message);
                    } if (throwable instanceof HttpException) {
                        HttpException httpException = (HttpException) throwable;
                        int errorCode = httpException.code();

                        handleError(errorCode);
                    } else {
                        mShowErrorEvent.setValue(R.string.server_error_message);
                    }
                });

        mCompositeDisposable.add(disposable);
    }

    private void handleError(int errorCode) {
        switch (errorCode) {
            case SOLUTION_NOT_FOUND_HTTP_ERROR_CODE:
            case NAN_INJECTION_RATES_HTTP_ERROR_CODE:
                mShowErrorEvent.setValue(R.string.solution_not_found_error_message);
                break;
        }
    }

    public void onShowNextCard() {
        getNextCardToRepeat();
    }

    private void getNextCardToRepeat() {
        Disposable disposable = mCardScheduler.checkIfMoreCardsToRepeatAvailable()
                .subscribeOn(mBackgroundScheduler)
                .observeOn(mMainScheduler)
                .map(isMoreCardsAvailable -> {
                    if (!isMoreCardsAvailable) {
                        mCardsFinishedEvent.call();
                    }

                    return isMoreCardsAvailable;
                }).filter(isMoreCardsAvailable -> isMoreCardsAvailable)
                .observeOn(mBackgroundScheduler)
                .flatMapSingleElement(isMoreCardsAvailable -> mCardScheduler.getNextCardToReview())
                .observeOn(mMainScheduler)
                .subscribe(card -> {
                    mCurrentCard = card;

                    mQuestionTextObservable.setValue(mCurrentCard.getQuestion());
                    //TODO: replace with mIsAnswerShownObservable so ui layer decides what that means exactly
                    mAnswerTextObservable.setValue("");
                }, throwable -> mLogger.e(TAG, "getNextCardToReview: " + throwable.getMessage()));

        mCompositeDisposable.add(disposable);
    }
}

package daniil.shevtsov.com.spacedrecall.service.repository.database;

import java.util.List;

import javax.inject.Inject;

import daniil.shevtsov.com.spacedrecall.api.room.model.dao.ReviewLogDao;
import daniil.shevtsov.com.spacedrecall.api.room.model.entity.ReviewLog;
import io.reactivex.Completable;
import io.reactivex.Single;

public class ReviewLogsRepository {
    private final ReviewLogDao mReviewLogDao;

    @Inject
    public ReviewLogsRepository(ReviewLogDao reviewLogDao) {
        mReviewLogDao = reviewLogDao;
    }

    public Completable insertReviewLogs(List<ReviewLog> reviewLogs) {
        return mReviewLogDao.insert(reviewLogs).toCompletable();
    }

    public Completable addNewReviewLog(ReviewLog reviewLog) {
        return mReviewLogDao.insert(reviewLog).toCompletable();
    }

    public Single<List<ReviewLog>> getReviewLogs() {
        return mReviewLogDao.selectAllReviewLogs();
    }

    public Single<Integer> countNewCardsInDeck(Long deckId, Long sessionLogId) {
        return mReviewLogDao.selectReviewCountForDeckWithSessionLogId(deckId, sessionLogId);
    }
}

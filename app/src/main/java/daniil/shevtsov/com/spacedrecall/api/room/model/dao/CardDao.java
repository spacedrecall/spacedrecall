package daniil.shevtsov.com.spacedrecall.api.room.model.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import daniil.shevtsov.com.spacedrecall.api.room.model.entity.Card;
import io.reactivex.Completable;
import io.reactivex.Single;

@Dao
public interface CardDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Single<Long> insert(Card card);

    @Insert
    Single<List<Long>> insert(List<Card> cards);

    @Query("SELECT * FROM cards_table LIMIT 1")
    Single<Card> selectOneCard();

    @Query("SELECT * FROM cards_table WHERE card_id = :cardId")
    Single<Card> selectCardWithId(Long cardId);

    @Query("SELECT * FROM cards_table ORDER BY first_added_timestamp ASC")
    Single<List<Card>> selectAllCards();

    @Query("SELECT * FROM cards_table WHERE cards_table.deck_id = :deckId ORDER BY last_repetition_date ASC LIMIT 1")
    Single<Card> selectEarliestCardFromDeck(long deckId);

    @Query("SELECT COUNT(*) FROM cards_table WHERE cards_table.deck_id = :deckId")
    Single<Integer> countCardsAvailableForRepetition(long deckId);

    @Query("UPDATE cards_table SET deck_id = :newDeckId WHERE card_id = :cardId")
    Completable updateCardDeckId(long cardId, long newDeckId);

    @Query("UPDATE cards_table SET last_repetition_date = :newLastRepetitionTime WHERE card_id = :cardId")
    Completable updateCardLastRepetitionTimestamp(Long cardId, long newLastRepetitionTime);

    @Query("DELETE FROM cards_table")
    void deleteAllCards();
}

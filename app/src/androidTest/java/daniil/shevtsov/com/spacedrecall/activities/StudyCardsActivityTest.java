package daniil.shevtsov.com.spacedrecall.activities;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleRegistry;
import androidx.lifecycle.ProcessLifecycleOwner;
import androidx.test.InstrumentationRegistry;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import javax.inject.Inject;

import daniil.shevtsov.com.spacedrecall.R;
import daniil.shevtsov.com.spacedrecall.android.SessionTracker;
import daniil.shevtsov.com.spacedrecall.api.room.model.entity.Card;
import daniil.shevtsov.com.spacedrecall.api.room.model.entity.Deck;
import daniil.shevtsov.com.spacedrecall.util.MockRequestDispatcher;
import daniil.shevtsov.com.spacedrecall.util.TestDatabaseUtil;
import daniil.shevtsov.com.spacedrecall.util.dagger.TestApplication;
import daniil.shevtsov.com.spacedrecall.view.ui.StudyCardsActivity;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.mockwebserver.MockWebServer;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class StudyCardsActivityTest {
    private static final List<Deck> DECKS = TestDatabaseUtil.getDecks();
    private static final List<Card> CARDS = TestDatabaseUtil.getCards();
    private static final String TEST_DATABASE_NAME = "study_cards_test_database";
    private static final long AWAIT_TIMEOUT = TestDatabaseUtil.getAwaitTimeout();

    @Inject
    TestDatabaseUtil mTestDatabaseUtil;

    @Inject
    SessionTracker mSessionTracker;

    private LifecycleRegistry mLifecycleRegistry;

    private MockWebServer mMockWebServer;

    @Rule
    public final ActivityTestRule<StudyCardsActivity> mActivityTestRule = new ActivityTestRule<>(
            StudyCardsActivity.class, false, false);

    @Before
    public void onSetup() throws IOException {
        TestApplication testApplication = (TestApplication) InstrumentationRegistry.getTargetContext().getApplicationContext();

        testApplication.buildTestAppComponent();

        testApplication.getAppComponent().inject(this);

        mMockWebServer = new MockWebServer();
        mMockWebServer.start(8080);

        mLifecycleRegistry = new LifecycleRegistry(ProcessLifecycleOwner.get());
        mLifecycleRegistry.addObserver(mSessionTracker);

        mLifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_START );

        mTestDatabaseUtil.setDatabaseInMemory(TEST_DATABASE_NAME);

        mTestDatabaseUtil.populateTestDatabase();

        mMockWebServer.setDispatcher(new MockRequestDispatcher());

        mActivityTestRule.launchActivity(null);
    }

    @After
    public void onTearDown() throws IOException {
        mTestDatabaseUtil.tearDownTestDatabase();
        mMockWebServer.shutdown();
    }

    @Test
    public void testRecallGradeButtonsHidden() {
        onView(withId(R.id.recall_grade_buttons_layout)).check(matches(not(isDisplayed())));
    }

    @Test
    public void testQuestionText() {
        String firstCardsQuestion = CARDS.get(0).getQuestion();
        onView(withId(R.id.study_cards_question_edit_text)).check(matches(withText(firstCardsQuestion)));
    }

    @Test
    public void testAnswerEmptyInitially() {
        onView(withId(R.id.study_cards_answer_edit_text)).check(matches(withText("")));
    }

    @Test
    public void testShowAnswerText() {
        onView(withId(R.id.show_answer_button)).perform(click());

        String firstCardAnswer = CARDS.get(0).getAnswer();
        onView(withId(R.id.study_cards_answer_edit_text)).check(matches(withText(firstCardAnswer)));
    }

    @Test
    public void testShowAnswerButtonHidden() {
        onView(withId(R.id.show_answer_button)).perform(click());

        onView(withId(R.id.show_answer_button)).check(matches(not(isDisplayed())));
    }

    @Test
    public void testShowAnswerGradeButtonsDisplayed() {
        onView(withId(R.id.show_answer_button)).perform(click());

        onView(withId(R.id.recall_grade_buttons_layout)).check(matches(isDisplayed()));
    }

    @Test
    public void testNextCardDisplayedNotRecalled() {
        onView(withId(R.id.show_answer_button)).perform(click());
        onView(withId(R.id.card_not_recalled_button)).perform(click());

        String secondCardQuestion = CARDS.get(1).getQuestion();
        onView(withId(R.id.study_cards_question_edit_text)).check(matches(withText(secondCardQuestion)));
    }

    @Test
    public void testNextCardDisplayedRecalled() {
        onView(withId(R.id.show_answer_button)).perform(click());
        onView(withId(R.id.card_recalled_button)).perform(click());

        String secondCardQuestion = CARDS.get(1).getQuestion();
        onView(withId(R.id.study_cards_question_edit_text)).check(matches(withText(secondCardQuestion)));
    }

    @Test
    public void testNextCardGradeButtonsHidden() {
        onView(withId(R.id.show_answer_button)).perform(click());
        onView(withId(R.id.card_recalled_button)).perform(click());

        onView(withId(R.id.recall_grade_buttons_layout)).check(matches(not(isDisplayed())));
    }

    @Test
    public void testNextCardAnswerHidden() {
        onView(withId(R.id.show_answer_button)).perform(click());
        onView(withId(R.id.card_recalled_button)).perform(click());

        onView(withId(R.id.study_cards_answer_edit_text)).check(matches(withText("")));
    }

    @Test
    public void testClosedWhenCardsNotAvailable() {
        reviewCards(CARDS.size());

        assertTrue(mActivityTestRule.getActivity().isFinishing());
    }

    @Test
    public void testInsertingCorrectReviewedCardsCount() {
        mTestDatabaseUtil.getCardsDatabase().sessionLogDao().deleteAllSessionLogs();

        int reviewedCardsCount = CARDS.size();

        reviewCards(reviewedCardsCount);

        mLifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_STOP);

        mTestDatabaseUtil.getCardsDatabase().sessionLogDao().selectAllSessionLogs()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .test()
                .awaitDone(AWAIT_TIMEOUT, TimeUnit.SECONDS)
                .assertValue(sessionLogs -> sessionLogs
                        .stream()
                        .anyMatch(sessionLog -> sessionLog.getReviewedCardsCount().equals(reviewedCardsCount)));
    }

    //TODO: Fix this horrible hack so there are no thread sleeps
    private void reviewCards(int nTimes) {
        IntStream.range(0, nTimes)
                .forEach(i -> {
                    try {
                        reviewCard();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                });


    }

    private void reviewCard() throws InterruptedException {
        int timeout = 500;

        Thread.sleep(timeout);
        onView(withId(R.id.show_answer_button)).perform(click());
        Thread.sleep(timeout);
        onView(withId(R.id.card_recalled_button)).perform(click());
    }

}

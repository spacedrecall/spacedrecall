package daniil.shevtsov.com.spacedrecall.util;


import org.jetbrains.annotations.NotNull;

import okhttp3.mockwebserver.Dispatcher;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.RecordedRequest;

public class MockRequestDispatcher extends Dispatcher {

    @NotNull
    @Override
    public MockResponse dispatch(@NotNull RecordedRequest recordedRequest) {
        return new MockResponse()
                .setResponseCode(200)
                .setBody("{" +
                        "\"serviceRates\":[0.9999999999994071,1.9221459640099563," +
                        "1.9221459640099579,1.9221459640099579,1.9221459640099579]," +
                        "\"arrivalRate\":0.0}");
    }
}

package daniil.shevtsov.com.spacedrecall.util.dagger;

import dagger.Module;
import dagger.Provides;
import daniil.shevtsov.com.spacedrecall.android.SessionTracker;
import daniil.shevtsov.com.spacedrecall.api.room.CardsDatabase;
import daniil.shevtsov.com.spacedrecall.dagger.scope.ApplicationScope;
import daniil.shevtsov.com.spacedrecall.service.model.statistics.SessionLogger;
import daniil.shevtsov.com.spacedrecall.util.TestDatabaseUtil;
import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

@Module
public class TestModule {
    @Provides
    String provideTestString() {
        return "lol";
    }

    @Provides
    @ApplicationScope
    Scheduler provideBackgroundScheduler() {
        return Schedulers.io();
    }

    @Provides
    @ApplicationScope
    SessionTracker provideSessionTracker(SessionLogger sessionLogger) {
        return new SessionTracker(sessionLogger);
    }

    @Provides
    @ApplicationScope
    TestDatabaseUtil provideDatabaseUtil(CardsDatabase cardsDatabase) {
        return new TestDatabaseUtil(cardsDatabase);
    }
}

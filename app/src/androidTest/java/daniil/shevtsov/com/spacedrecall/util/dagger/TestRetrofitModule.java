package daniil.shevtsov.com.spacedrecall.util.dagger;

import dagger.Module;
import dagger.Provides;
import daniil.shevtsov.com.spacedrecall.dagger.module.CommonRetrofitModule;
import daniil.shevtsov.com.spacedrecall.dagger.scope.ApplicationScope;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(includes = CommonRetrofitModule.class)
public class TestRetrofitModule {
    @Provides
    @ApplicationScope
    Retrofit provideRetrofit(OkHttpClient okHttpClient, GsonConverterFactory gsonConverterFactory) {
        return new Retrofit.Builder()
                .addConverterFactory(gsonConverterFactory)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl("http://localhost:8080/")
                .build();
    }


}

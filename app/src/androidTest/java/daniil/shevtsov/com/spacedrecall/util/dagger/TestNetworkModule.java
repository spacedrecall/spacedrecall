package daniil.shevtsov.com.spacedrecall.util.dagger;

import dagger.Module;
import dagger.Provides;
import daniil.shevtsov.com.spacedrecall.api.retrofit.ApiClient;
import daniil.shevtsov.com.spacedrecall.api.retrofit.RestApiInterface;
import daniil.shevtsov.com.spacedrecall.dagger.scope.ApplicationScope;

/*TODO: Fix dependency tree: test and production networkModule are
    the same but need to include different retrofit modules*/
@Module(includes = TestRetrofitModule.class)
public class TestNetworkModule {
    @Provides
    @ApplicationScope
    ApiClient provideApiClient(RestApiInterface restApiInterface) {
        return new ApiClient(restApiInterface);
    }
}

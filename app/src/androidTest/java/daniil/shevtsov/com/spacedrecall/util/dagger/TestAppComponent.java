package daniil.shevtsov.com.spacedrecall.util.dagger;

import dagger.Component;
import daniil.shevtsov.com.spacedrecall.activities.AddCardsActivityTest;
import daniil.shevtsov.com.spacedrecall.activities.MainActivityTest;
import daniil.shevtsov.com.spacedrecall.activities.StudyCardsActivityTest;
import daniil.shevtsov.com.spacedrecall.api.room.model.dao.CardDaoTest;
import daniil.shevtsov.com.spacedrecall.api.room.model.dao.DeckDaoTest;
import daniil.shevtsov.com.spacedrecall.api.room.model.dao.ReviewLogDaoTest;
import daniil.shevtsov.com.spacedrecall.api.room.model.dao.SessionLogDaoTest;
import daniil.shevtsov.com.spacedrecall.dagger.component.AppComponent;
import daniil.shevtsov.com.spacedrecall.dagger.module.AppModule;
import daniil.shevtsov.com.spacedrecall.dagger.module.CommonModule;
import daniil.shevtsov.com.spacedrecall.dagger.scope.ApplicationScope;

@ApplicationScope
@Component(modules = {TestModule.class, AppModule.class, CommonModule.class, TestRoomModule.class,
        TestNetworkModule.class})
public interface TestAppComponent extends AppComponent {
    void inject(MainActivityTest mainActivityTest);

    void inject(StudyCardsActivityTest studyCardsActivityTest);

    void inject(AddCardsActivityTest addCardsActivityTest);

    void inject(CardDaoTest cardDaoTest);

    void inject(DeckDaoTest deckDaoTest);

    void inject(ReviewLogDaoTest reviewLogDaoTest);

    void inject(SessionLogDaoTest sessionLogDaoTest);
}

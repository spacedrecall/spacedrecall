package daniil.shevtsov.com.spacedrecall.util;

import androidx.sqlite.db.SupportSQLiteDatabase;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import daniil.shevtsov.com.spacedrecall.BuildConfig;
import daniil.shevtsov.com.spacedrecall.api.room.CardsDatabase;
import daniil.shevtsov.com.spacedrecall.api.room.model.entity.Card;
import daniil.shevtsov.com.spacedrecall.api.room.model.entity.Deck;
import daniil.shevtsov.com.spacedrecall.api.room.model.entity.ReviewLog;
import daniil.shevtsov.com.spacedrecall.api.room.model.entity.SessionLog;

public class TestDatabaseUtil {
    private static final long AWAIT_TIMEOUT = 5L;

    private static final Deck FIRST_DECK = new Deck(1, 12L, 1L, 13L);
    private static final Deck SECOND_DECK = new Deck(2, 21L, 3L, 24L);
    private static final Deck THIRD_DECK = new Deck(3, 22L, 4L, 26L);
    private static final Deck FOURTH_DECK= new Deck(4, 23L, 5L, 28L);
    private static final Deck FIFTH_DECK = new Deck(5, 24L, 6L, 30L);
    private static final List<Deck> DECKS = Arrays.asList(
            FIRST_DECK,
            SECOND_DECK,
            THIRD_DECK,
            FOURTH_DECK,
            FIFTH_DECK
    );

    private static final Card FIRST_CARD = new Card("Dog", "Собака", 3L, null, 1L);
    private static final Card SECOND_CARD = new Card("Cat", "Кошка", 4L, null, 2L);
    private static final Card THIRD_CARD = new Card("Car", "Машина", 5L, null, 3L);

    private static final List<Card> CARDS = Arrays.asList(
            FIRST_CARD,
            SECOND_CARD,
            THIRD_CARD
    );
    private static final SessionLog FIRST_SESSION_LOG = new SessionLog(2L,
            3L, 4, 5, 6, 10.0);
    private static final SessionLog SECOND_SESSION_LOG = new SessionLog(3L,
            4L, 5, 6, 7, 10.0);

    private static final List<SessionLog> SESSION_LOGS = Arrays.asList(
            FIRST_SESSION_LOG,
            SECOND_SESSION_LOG
    );

    private static final ReviewLog FIRST_REVIEW_LOG = new ReviewLog(null,
            null, 3L);
    private static final ReviewLog SECOND_REVIEW_LOG = new ReviewLog(null,
            null, 5L);

    private static final List<ReviewLog> REVIEW_LOGS = Arrays.asList(
            FIRST_REVIEW_LOG,
            SECOND_REVIEW_LOG
    );

    private final CardsDatabase mCardsDatabase;

    @Inject
    public TestDatabaseUtil(CardsDatabase cardsDatabase) {
        mCardsDatabase = cardsDatabase;
    }

    public CardsDatabase getCardsDatabase() {
        return mCardsDatabase;
    }

    public void setDatabaseInMemory(String databaseName) {
        makeInMemoryDatabaseVisibleToDebugDb(databaseName, mCardsDatabase.getOpenHelper().getReadableDatabase());
    }


    public void populateTestDatabase() {
        //TODO: find a more elegant way to prepopulate test database with values
        populateDecks();
        populateCards();
        populateSessionLogs();
        populateReviewLogs();
    }

    private void populateDecks() {
        for (Deck deck : DECKS) {
            long deckId = mCardsDatabase.deckDao().insert(deck)
                    .blockingGet();
            deck.setDeckId(deckId);
        }
    }

    private void populateCards() {
        for (Card card : CARDS) {
            card.setDeckId(FIRST_DECK.getDeckId());
            long cardId = mCardsDatabase.cardDao().insert(card)
                    .blockingGet();

            card.setCardId(cardId);
        }
    }

    private void populateSessionLogs() {
        for(SessionLog sessionLog : SESSION_LOGS) {
            long sessionLogId = mCardsDatabase.sessionLogDao().insert(sessionLog)
                    .blockingGet();

            sessionLog.setSessionLogId(sessionLogId);
        }
    }

    private void populateReviewLogs() {
        for(ReviewLog reviewLog : REVIEW_LOGS) {
            reviewLog.setSessionLogId(FIRST_SESSION_LOG.getSessionLogId());
            reviewLog.setNewDeckId(FIRST_DECK.getDeckId());

            long reviewLogId = mCardsDatabase.reviewLogDao().insert(reviewLog)
                    .blockingGet();

            reviewLog.setReviewLogId(reviewLogId);
        }
    }

    public void tearDownTestDatabase() {
        mCardsDatabase.clearAllTables();
        mCardsDatabase.close();
    }

    public static long getAwaitTimeout() {
        return AWAIT_TIMEOUT;
    }

    public static Deck getFirstDeck() {
        return FIRST_DECK;
    }

    public static Deck getSecondDeck() {
        return SECOND_DECK;
    }

    public static List<Deck> getDecks() {
        return DECKS;
    }

    public static Card getFirstCard() {
        return FIRST_CARD;
    }

    public static Card getSecondCard() {
        return SECOND_CARD;
    }

    public static List<Card> getCards() {
        return CARDS;
    }

    public static SessionLog getFirstSessionLog() {
        return FIRST_SESSION_LOG;
    }

    public static SessionLog getSecondSessionLog() {
        return SECOND_SESSION_LOG;
    }

    public static List<SessionLog> getSessionLogs() {
        return SESSION_LOGS;
    }

    public static ReviewLog getFirstReviewLog() {
        return FIRST_REVIEW_LOG;
    }

    public static ReviewLog getSecondReviewLog() {
        return SECOND_REVIEW_LOG;
    }

    public static List<ReviewLog> getReviewLogs() {
        return REVIEW_LOGS;
    }

    private static void makeInMemoryDatabaseVisibleToDebugDb(String testDatabaseName, SupportSQLiteDatabase... database) {
        if (BuildConfig.DEBUG) {
            try {
                Class<?> debugDB = Class.forName("com.amitshekhar.DebugDB");
                Class[] argTypes = new Class[]{HashMap.class};
                HashMap<String, SupportSQLiteDatabase> inMemoryDatabases = new HashMap<>();
                // set your inMemory databases
                inMemoryDatabases.put("InMemory: " + testDatabaseName, database[0]);
                Method setRoomInMemoryDatabase = debugDB.getMethod("setInMemoryRoomDatabases", argTypes);
                setRoomInMemoryDatabase.invoke(null, inMemoryDatabases);
            } catch (Exception ignore) {

            }
        }
    }
}

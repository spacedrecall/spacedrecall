package daniil.shevtsov.com.spacedrecall.api.room.model.dao;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.test.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import daniil.shevtsov.com.spacedrecall.api.room.model.entity.Deck;
import daniil.shevtsov.com.spacedrecall.api.room.model.entity.ReviewLog;
import daniil.shevtsov.com.spacedrecall.util.TestDatabaseUtil;
import daniil.shevtsov.com.spacedrecall.util.dagger.TestApplication;
import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

@RunWith(AndroidJUnit4.class)
public class ReviewLogDaoTest {
    private static final long AWAIT_TIMEOUT = TestDatabaseUtil.getAwaitTimeout();

    private static final ReviewLog FIRST_REVIEW_LOG = TestDatabaseUtil.getFirstReviewLog();
    private static final ReviewLog SECOND_REVIEW_LOG = TestDatabaseUtil.getSecondReviewLog();

    private static final Deck FIRST_DECK = TestDatabaseUtil.getFirstDeck();

    private static final List<ReviewLog> REVIEW_LOGS = TestDatabaseUtil.getReviewLogs();

    @Rule
    public TestRule rule = new InstantTaskExecutorRule();

    @Inject
    TestDatabaseUtil mTestDatabaseUtil;

    private Scheduler mTestScheduler;

    private ReviewLogDao mReviewLogDao;

    @Before
    public void onSetup() {
        mTestScheduler = Schedulers.io();

        TestApplication testApplication = (TestApplication) InstrumentationRegistry.getTargetContext().getApplicationContext();

        testApplication.buildTestAppComponent();

        testApplication.getAppComponent().inject(this);

        mReviewLogDao = mTestDatabaseUtil.getCardsDatabase().reviewLogDao();

        mTestDatabaseUtil.populateTestDatabase();
    }

    @After
    public void onTearDown() {
        mTestDatabaseUtil.tearDownTestDatabase();
    }

    @Test
    public void testSelectReviewLogWithId() {
        mReviewLogDao.selectReviewLogWithId(FIRST_REVIEW_LOG.getReviewLogId())
                .subscribeOn(mTestScheduler)
                .test()
                .awaitDone(AWAIT_TIMEOUT, TimeUnit.SECONDS)
                .assertValue(reviewLog ->
                        reviewLog.getTimestamp().equals(FIRST_REVIEW_LOG.getTimestamp()));
    }

    @Test
    public void testInsertReviewLogList() {
        mReviewLogDao.insert(REVIEW_LOGS)
                .flatMap(reviewLogIds -> mReviewLogDao.selectAllReviewLogs())
                .test()
                .awaitDone(AWAIT_TIMEOUT, TimeUnit.SECONDS)
                .assertValue(reviewLogs -> reviewLogs.get(0).getTimestamp().equals(REVIEW_LOGS.get(0).getTimestamp())
                        && reviewLogs.get(1).getTimestamp().equals(REVIEW_LOGS.get(1).getTimestamp()));
    }

    @Test
    public void testSelectAllReviewLogs() {
        mReviewLogDao.selectAllReviewLogs()
                .test()
                .awaitDone(AWAIT_TIMEOUT, TimeUnit.SECONDS)
                .assertValue(reviewLogs -> reviewLogs.get(0).getTimestamp().equals(REVIEW_LOGS.get(0).getTimestamp())
                        && reviewLogs.get(1).getTimestamp().equals(REVIEW_LOGS.get(1).getTimestamp()));
    }

    @Test
    public void testSelectReviewCountForDeckWithSessionLogId() {
        mReviewLogDao.selectReviewCountForDeckWithSessionLogId(FIRST_DECK.getDeckId(), FIRST_REVIEW_LOG.getSessionLogId())
                .subscribeOn(mTestScheduler)
                .test()
                .awaitDone(AWAIT_TIMEOUT, TimeUnit.SECONDS)
                .assertValue(REVIEW_LOGS.size());
    }
}

package daniil.shevtsov.com.spacedrecall.util.dagger;

import android.util.Log;

import daniil.shevtsov.com.spacedrecall.android.MyApplication;
import daniil.shevtsov.com.spacedrecall.dagger.module.AppModule;
import daniil.shevtsov.com.spacedrecall.dagger.module.CommonModule;

public class TestApplication extends MyApplication {
    private static final String TAG = TestApplication.class.getSimpleName();

    private TestAppComponent mTestAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        Log.d(TAG, "TestApplication onCreate");

        buildTestAppComponent();
    }

     /*Hack(?) to isolate every test.
     All tests use shared @ApplicationScope database
     if this method is not called in setup*/

    public void buildTestAppComponent() {
        mTestAppComponent = DaggerTestAppComponent.builder()
                .commonModule(new CommonModule())
                .testRoomModule(new TestRoomModule())
                .testNetworkModule(new TestNetworkModule())
                .appModule(new AppModule(this))
                .testModule(new TestModule())
                .build();
    }

    @Override
    public TestAppComponent getAppComponent() {
        Log.d(TAG, "TestApplication getAppComponent");
        return mTestAppComponent;
    }
}

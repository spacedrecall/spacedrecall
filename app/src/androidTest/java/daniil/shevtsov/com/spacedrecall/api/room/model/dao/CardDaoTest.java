package daniil.shevtsov.com.spacedrecall.api.room.model.dao;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.test.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import daniil.shevtsov.com.spacedrecall.api.room.model.entity.Card;
import daniil.shevtsov.com.spacedrecall.api.room.model.entity.Deck;
import daniil.shevtsov.com.spacedrecall.util.TestDatabaseUtil;
import daniil.shevtsov.com.spacedrecall.util.dagger.TestApplication;
import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

@RunWith(AndroidJUnit4.class)
public class CardDaoTest {
    private static final long AWAIT_TIME_OUT = 5;

    private static final Deck FIRST_DECK = TestDatabaseUtil.getFirstDeck();
    private static final Deck MIDDLE_DECK = TestDatabaseUtil.getSecondDeck();

    private static final Card FIRST_CARD = TestDatabaseUtil.getFirstCard();
    private static final Card SECOND_CARD = TestDatabaseUtil.getSecondCard();

    private static final long CURRENT_TIMESTAMP = 15L;

    @Rule
    public TestRule rule = new InstantTaskExecutorRule();

    @Inject
    TestDatabaseUtil mTestDatabaseUtil;

    private Scheduler mTestScheduler;

    private CardDao mCardDao;

    private DeckDao mDeckDao;

    @Before
    public void onSetup() {
        mTestScheduler = Schedulers.io();

        TestApplication testApplication = (TestApplication) InstrumentationRegistry.getTargetContext().getApplicationContext();

        testApplication.buildTestAppComponent();

        testApplication.getAppComponent().inject(this);

        mCardDao = mTestDatabaseUtil.getCardsDatabase().cardDao();
        mDeckDao = mTestDatabaseUtil.getCardsDatabase().deckDao();

        mTestDatabaseUtil.populateTestDatabase();
    }

    @After
    public void onTearDown() {
        mTestDatabaseUtil.tearDownTestDatabase();
    }

    @Test
    public void testInsertAndSelectCard() {
        mCardDao.insert(FIRST_CARD)
                .subscribeOn(mTestScheduler)
                .flatMap(cardId -> mCardDao.selectOneCard())
                .test()
                .awaitDone(AWAIT_TIME_OUT, TimeUnit.SECONDS)
                .assertValue(card -> card.getQuestion().equals(FIRST_CARD.getQuestion()));
    }

    @Test
    public void testGetCardWithId() {
        mCardDao.selectCardWithId(FIRST_CARD.getCardId())
                .subscribeOn(mTestScheduler)
                .test()
                .awaitDone(AWAIT_TIME_OUT, TimeUnit.SECONDS)
                .assertValue(card -> card.getQuestion().equals(FIRST_CARD.getQuestion()));
    }

    @Test
    public void testGetAllCards() {
        mCardDao.selectAllCards()
                .subscribeOn(mTestScheduler)
                .test()
                .awaitDone(AWAIT_TIME_OUT, TimeUnit.SECONDS)
                .assertValue(cards -> cards.get(0).getQuestion().equals(FIRST_CARD.getQuestion())
                        && cards.get(1).getQuestion().equals(SECOND_CARD.getQuestion()));
    }

    @Test
    public void testSelectEarliestCardFromDeck() {
        mCardDao.selectEarliestCardFromDeck(FIRST_DECK.getDeckId())
                .subscribeOn(mTestScheduler)
                .test()
                .awaitDone(AWAIT_TIME_OUT, TimeUnit.SECONDS)
                .assertValue(card -> card.getQuestion().equals(FIRST_CARD.getQuestion()));
    }

    @Test
    public void testUpdateCardDeckId() {
        mCardDao.updateCardDeckId(FIRST_CARD.getCardId(), MIDDLE_DECK.getDeckId())
                .andThen(mCardDao.selectCardWithId(FIRST_CARD.getCardId()))
                .subscribeOn(mTestScheduler)
                .test()
                .awaitDone(AWAIT_TIME_OUT, TimeUnit.SECONDS)
                .assertValue(card -> card.getDeckId().equals(MIDDLE_DECK.getDeckId()));
    }

    @Test
    public void testUpdateCardLastRepetitionTimestamp() {
        mCardDao.insert(FIRST_CARD)
                .flatMap(cardId -> mCardDao.selectOneCard())
                .flatMap(card -> mCardDao.updateCardLastRepetitionTimestamp(card.getCardId(), CURRENT_TIMESTAMP).toSingleDefault(card))
                .flatMap(card -> mCardDao.selectOneCard())
                .subscribeOn(mTestScheduler)
                .test()
                .awaitDone(AWAIT_TIME_OUT, TimeUnit.SECONDS)
                .assertValue(card -> card.getQuestion().equals(FIRST_CARD.getQuestion()));
    }


}

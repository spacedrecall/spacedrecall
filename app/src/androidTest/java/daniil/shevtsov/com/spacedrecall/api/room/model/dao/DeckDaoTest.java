package daniil.shevtsov.com.spacedrecall.api.room.model.dao;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.test.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import daniil.shevtsov.com.spacedrecall.api.room.model.entity.Deck;
import daniil.shevtsov.com.spacedrecall.util.TestDatabaseUtil;
import daniil.shevtsov.com.spacedrecall.util.dagger.TestApplication;
import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

@RunWith(AndroidJUnit4.class)
public class DeckDaoTest {
    private static final long AWAIT_TIMEOUT = 5;

    private static final Deck TEST_FIRST_DECK = TestDatabaseUtil.getFirstDeck();
    private static final Deck TEST_SECOND_DECK = TestDatabaseUtil.getSecondDeck();

    private static final List<Deck> TEST_DECKS = TestDatabaseUtil.getDecks();

    @Inject
    TestDatabaseUtil mTestDatabaseUtil;

    private Scheduler mTestScheduler;
    @Rule
    public TestRule mTestRule = new InstantTaskExecutorRule();

    private DeckDao mDeckDao;

    @Before
    public void onSetup() {
        mTestScheduler = Schedulers.io();

        TestApplication testApplication = (TestApplication) InstrumentationRegistry.getTargetContext().getApplicationContext();

        testApplication.buildTestAppComponent();

        testApplication.getAppComponent().inject(this);

        mDeckDao = mTestDatabaseUtil.getCardsDatabase().deckDao();
    }


    @After
    public void onTearDown() {
        mTestDatabaseUtil.tearDownTestDatabase();
    }

    @Test
    public void testSelectOneDeck() {
        mDeckDao.insert(TEST_FIRST_DECK)
                .subscribeOn(mTestScheduler)
                .flatMap(deckId -> mDeckDao.selectOneDeck())
                .test()
                .awaitDone(AWAIT_TIMEOUT, TimeUnit.SECONDS)
                .assertValue(deck -> deck.getNumber().equals(TEST_FIRST_DECK.getNumber()));
    }

    @Test
    public void testSelectAllDecks() {
        mDeckDao.insert(TEST_DECKS)
                .flatMap(deckIds -> mDeckDao.selectAllDecks())
                .test()
                .assertValue(decks -> decks.get(0).getNumber().equals(TEST_FIRST_DECK.getNumber())
                        && decks.get(1).getNumber().equals(TEST_SECOND_DECK.getNumber()));
    }

    @Test
    public void testSelectDeckWithId() {
        mDeckDao.insert(TEST_FIRST_DECK)
                .subscribeOn(mTestScheduler)
                .flatMap(deckId -> mDeckDao.selectDeckWithId(deckId))
                .test()
                .awaitDone(AWAIT_TIMEOUT, TimeUnit.SECONDS)
                .assertValue(deck -> deck.getNumber().equals(TEST_FIRST_DECK.getNumber()));
    }

    @Test
    public void testSelectDeckWithNumber() {
        mDeckDao.insert(TEST_FIRST_DECK)
                .subscribeOn(mTestScheduler)
                .flatMap(deckId -> mDeckDao.selectDeckWithNumber(TEST_FIRST_DECK.getNumber()))
                .test()
                .awaitDone(AWAIT_TIMEOUT, TimeUnit.SECONDS)
                .assertValue(deck -> deck.getNumber().equals(TEST_FIRST_DECK.getNumber()));
    }

    @Test
    public void testSelectDeckCount() {
        mDeckDao.insert(TEST_DECKS)
                .flatMap(deckIds -> mDeckDao.selectDecksCount())
                .test()
                .assertValue(TEST_DECKS.size());
    }

    @Test
    public void testSelectDecksToRepeat() {
        long currentTImestamp = 14L;
        long firstTimestamp = 10L;
        long secondTimestamp = 20L;

        List<Deck> testRepetitionDecks = new ArrayList<>(TEST_DECKS);
        testRepetitionDecks.get(0).setNextRepetitionTimestamp(firstTimestamp);
        testRepetitionDecks.get(1).setNextRepetitionTimestamp(secondTimestamp);

        mDeckDao.insert(testRepetitionDecks)
                .flatMap(decksIds -> mDeckDao.selectDecksToRepeat(currentTImestamp))
                .test()
                .awaitDone(AWAIT_TIMEOUT, TimeUnit.SECONDS)
                .assertValue(decks -> decks.size() == 1
                        && decks.get(0).getNextRepetitionTimestamp().equals(firstTimestamp));
    }

    @Test
    public void testInsertAndSelectFirstDeck() {
        mDeckDao.insert(TEST_FIRST_DECK)
                .observeOn(mTestScheduler)
                .flatMap(deckId -> mDeckDao.selectFirstDeck())
                .test()
                .awaitDone(AWAIT_TIMEOUT, TimeUnit.SECONDS)
                .assertValue(deck -> deck.getNumber().equals(TEST_FIRST_DECK.getNumber()));
    }

    @Test
    public void testUpdateRepetitionInterval() {
        long newRepetitionInterval = 50L;

        mDeckDao.insert(TEST_FIRST_DECK)
                .subscribeOn(mTestScheduler)
                .flatMap(deckId -> mDeckDao.updateRepetitionInterval(deckId, newRepetitionInterval).toSingleDefault(deckId))
                .flatMap(deckId -> mDeckDao.selectDeckWithId(deckId))
                .test()
                .awaitDone(AWAIT_TIMEOUT, TimeUnit.SECONDS)
                .assertValue(deck -> deck.getRepetitionInterval().equals(newRepetitionInterval));
    }
}

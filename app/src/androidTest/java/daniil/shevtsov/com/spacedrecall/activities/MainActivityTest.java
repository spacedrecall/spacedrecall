package daniil.shevtsov.com.spacedrecall.activities;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleRegistry;
import androidx.lifecycle.ProcessLifecycleOwner;
import androidx.test.InstrumentationRegistry;
import androidx.test.espresso.Espresso;
import androidx.test.espresso.intent.rule.IntentsTestRule;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

import daniil.shevtsov.com.spacedrecall.R;
import daniil.shevtsov.com.spacedrecall.android.SessionTracker;
import daniil.shevtsov.com.spacedrecall.util.TestDatabaseUtil;
import daniil.shevtsov.com.spacedrecall.util.dagger.TestApplication;
import daniil.shevtsov.com.spacedrecall.view.ui.AddCardsActivity;
import daniil.shevtsov.com.spacedrecall.view.ui.MainActivity;
import daniil.shevtsov.com.spacedrecall.view.ui.StudyCardsActivity;

import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.intent.Intents.intended;
import static androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {
    private static final String TEST_DATABASE_NAME = "main_activity_test_database";

    @Rule
    public final IntentsTestRule<MainActivity> mIntentsTestRule = new IntentsTestRule<>(
            MainActivity.class, false, false);

    @Inject
    TestDatabaseUtil mTestDatabaseUtil;

    @Inject
    SessionTracker mSessionTracker;

    private LifecycleRegistry mLifecycleRegistry;

    @Before
    public void onSetup() {
        TestApplication testApplication = (TestApplication) InstrumentationRegistry.getTargetContext().getApplicationContext();

        testApplication.buildTestAppComponent();

        testApplication.getAppComponent().inject(this);

        mLifecycleRegistry = new LifecycleRegistry(ProcessLifecycleOwner.get());
        mLifecycleRegistry.addObserver(mSessionTracker);

        mLifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_START );

        mTestDatabaseUtil.setDatabaseInMemory(TEST_DATABASE_NAME);

        mIntentsTestRule.launchActivity(null);
    }

    @After
    public void onTearDown() {
        mTestDatabaseUtil.tearDownTestDatabase();
    }


    @Test
    public void testStudyActivityTransition() {
        Espresso.onView(ViewMatchers.withId(R.id.study_cards_transition_button))
                .perform(click());
        intended(hasComponent(StudyCardsActivity.class.getName()));
    }

    @Test
    public void testAddCardsActivityTransition() {
        Espresso.onView(withId(R.id.add_cards_transition_button))
                .perform(click());
        intended(hasComponent(AddCardsActivity.class.getName()));
    }
}

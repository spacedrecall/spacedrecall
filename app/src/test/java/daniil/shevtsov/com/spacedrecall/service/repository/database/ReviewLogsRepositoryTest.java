package daniil.shevtsov.com.spacedrecall.service.repository.database;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import daniil.shevtsov.com.spacedrecall.api.room.model.dao.ReviewLogDao;
import daniil.shevtsov.com.spacedrecall.api.room.model.entity.ReviewLog;
import daniil.shevtsov.com.spacedrecall.util.TestDataProvider;
import io.reactivex.Single;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

public class ReviewLogsRepositoryTest {
    private static final List<ReviewLog> REVIEW_LOGS = TestDataProvider.getTestReviewLogs();
    private static final ReviewLog FIRST_REVIEW_LOG = TestDataProvider.getTestFirstReviewLog();

    @Mock
    private ReviewLogDao mReviewLogDao;

    private ReviewLogsRepository mReviewLogsRepository;

    @Before
    public void onSetup() {
        MockitoAnnotations.initMocks(this);

        mReviewLogsRepository = new ReviewLogsRepository(mReviewLogDao);
    }

    @Test
    public void testAddNewReviewLog() {
        doReturn(Single.just(FIRST_REVIEW_LOG.getReviewLogId())).when(mReviewLogDao).insert(FIRST_REVIEW_LOG);

        mReviewLogsRepository.addNewReviewLog(FIRST_REVIEW_LOG)
                .test()
                .onComplete();

        verify(mReviewLogDao).insert(FIRST_REVIEW_LOG);
    }

    @Test
    public void testGetReviewLogs() {
        doReturn(Single.just(REVIEW_LOGS)).when(mReviewLogDao).selectAllReviewLogs();

        mReviewLogsRepository.getReviewLogs()
                .test()
                .assertValue(REVIEW_LOGS);
    }
}

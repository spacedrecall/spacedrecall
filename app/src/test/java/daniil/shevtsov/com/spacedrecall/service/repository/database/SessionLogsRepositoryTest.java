package daniil.shevtsov.com.spacedrecall.service.repository.database;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import daniil.shevtsov.com.spacedrecall.api.room.model.dao.SessionLogDao;
import daniil.shevtsov.com.spacedrecall.api.room.model.entity.SessionLog;
import daniil.shevtsov.com.spacedrecall.util.TestDataProvider;
import io.reactivex.Single;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

public class SessionLogsRepositoryTest {
    private static final SessionLog FIRST_SESSION_LOG = TestDataProvider.getTestFirstSessionLog();

    private static final List<SessionLog> SESSION_LOGS = TestDataProvider.getTestSessionLogs();

    @Mock
    SessionLogDao mSessionLogDao;

    private SessionLogsRepository mSessionLogsRepository;

    @Before
    public void onSetup() {
        MockitoAnnotations.initMocks(this);

        mSessionLogsRepository = new SessionLogsRepository(mSessionLogDao);
    }

    @Test
    public void testAddNewSessionLog() {
        doReturn(Single.just(FIRST_SESSION_LOG.getSessionLogId()))
                .when(mSessionLogDao).insert(FIRST_SESSION_LOG);

        mSessionLogsRepository.addNewSessionLog(FIRST_SESSION_LOG)
                .test()
                .onComplete();

        verify(mSessionLogDao).insert(FIRST_SESSION_LOG);
    }

    @Test
    public void testGetSessionLogs() {
        doReturn(Single.just(SESSION_LOGS)).when(mSessionLogDao).selectAllSessionLogs();

        mSessionLogsRepository.getSessionLogs()
                .test()
                .assertValue(SESSION_LOGS);
    }
}

package daniil.shevtsov.com.spacedrecall.service.model.schedule;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import daniil.shevtsov.com.spacedrecall.api.room.model.entity.Card;
import daniil.shevtsov.com.spacedrecall.api.room.model.entity.Deck;
import daniil.shevtsov.com.spacedrecall.service.repository.TimestampProvider;
import daniil.shevtsov.com.spacedrecall.service.repository.database.CardProvider;
import daniil.shevtsov.com.spacedrecall.service.repository.database.DeckProvider;
import daniil.shevtsov.com.spacedrecall.util.Logger;
import daniil.shevtsov.com.spacedrecall.util.TestDataProvider;
import io.reactivex.Completable;
import io.reactivex.Single;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

public class CardSchedulerTest {
    private static final Deck TEST_FIRST_DECK = TestDataProvider.getTestFirstDeck();
    private static final Deck TEST_MIDDLE_DECK = TestDataProvider.getTestCurrentDeck();

    private static final Card TEST_FIRST_CARD = TestDataProvider.getTestFirstCard();

    private static final List<Card> CARDS = TestDataProvider.getTestCards();

    private static final List<Card> EMPTY_CARDS_LIST = new ArrayList<>();

    private static final long CURRENT_TIMESTAMP = 10L;

    private CardScheduler mCardScheduler;

    @Mock
    private DeckProvider mDeckProvider;

    @Mock
    private CardProvider mCardProvider;

    @Mock
    private NextDeckCalculator mNextDeckCalculator;

    @Mock
    private ReviewScheduleCalculator mReviewScheduleCalculator;

    @Mock
    private TimestampProvider mTimestampProvider;

    @Mock
    private Logger mLogger;

    @Before
    public void onSetup() {
        MockitoAnnotations.initMocks(this);

        doReturn(CURRENT_TIMESTAMP).when(mTimestampProvider).getCurrentTimestamp();
        doReturn(Single.just(TEST_MIDDLE_DECK)).when(mDeckProvider).getEarliestDeck();

        mCardScheduler = new CardScheduler(mDeckProvider, mCardProvider, mTimestampProvider,
                mNextDeckCalculator, mReviewScheduleCalculator, mLogger);
    }

    @Test
    public void testRecalculateServiceRates() {
        doReturn(Completable.complete()).when(mCardProvider).updateLastRepetitionTime(TEST_FIRST_CARD.getCardId(), CURRENT_TIMESTAMP);
        doReturn(Completable.complete()).when(mReviewScheduleCalculator).updateServiceRates();

        mCardScheduler.recalculateServiceRates(TEST_FIRST_CARD)
                .blockingAwait();

        verify(mCardProvider).updateLastRepetitionTime(TEST_FIRST_CARD.getCardId(), CURRENT_TIMESTAMP);

        verify(mReviewScheduleCalculator).updateServiceRates();
    }

    @Test
    public void testMoveCardToAnotherDeck() {
        //TODO: this class has too many responsibilities
        doReturn(Single.just(TEST_FIRST_DECK)).when(mDeckProvider).getDeckForId(TEST_FIRST_DECK.getDeckId());
        doReturn(TEST_MIDDLE_DECK.getNumber()).when(mNextDeckCalculator)
                .calculateDeckNumber(TEST_FIRST_DECK.getNumber(), true);
        doReturn(Single.just(TEST_MIDDLE_DECK)).when(mDeckProvider).getDeckForNumber(TEST_MIDDLE_DECK.getNumber());
        doReturn(Completable.complete()).when(mCardProvider).moveCardToAnotherDeck(TEST_FIRST_CARD, TEST_MIDDLE_DECK.getDeckId());


        mCardScheduler.moveCardToAnotherDeck(TEST_FIRST_CARD, true)
                .test()
                .onComplete();

        verify(mCardProvider).moveCardToAnotherDeck(TEST_FIRST_CARD, TEST_MIDDLE_DECK.getDeckId());
    }

    @Test
    public void testGetNextCardToReview() {
        doReturn(Single.just(TEST_FIRST_CARD)).when(mCardProvider).getEarliestCardFromDeck(TEST_MIDDLE_DECK.getDeckId());

        mCardScheduler.getNextCardToReview()
                .test()
                .assertValue(TEST_FIRST_CARD);
    }

    @Test
    public void testIsCardAvailableHasCards() {
        doReturn(Single.just(2)).when(mCardProvider).countCardsAvailableForRepetition(TEST_MIDDLE_DECK.getDeckId());

        mCardScheduler.checkIfMoreCardsToRepeatAvailable()
                .test()
                .assertValue(true);
    }

    @Test
    public void testIsCardAvailableNoCards() {
        doReturn(Single.just(0)).when(mCardProvider).countCardsAvailableForRepetition(TEST_MIDDLE_DECK.getDeckId());

        mCardScheduler.checkIfMoreCardsToRepeatAvailable()
                .test()
                .assertValue(false);
    }
}

package daniil.shevtsov.com.spacedrecall.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import daniil.shevtsov.com.spacedrecall.api.room.model.entity.Card;
import daniil.shevtsov.com.spacedrecall.api.room.model.entity.Deck;
import daniil.shevtsov.com.spacedrecall.api.room.model.entity.ReviewLog;
import daniil.shevtsov.com.spacedrecall.api.room.model.entity.SessionLog;

public class TestDataProvider {
    private static final Deck TEST_FIRST_DECK = new Deck( 1L, 1, 10L, 1L, 11L);
    private static final Deck TEST_CURRENT_DECK = new Deck( 2L, 2, 20L, 2L, 20L);
    private static final Deck TEST_LAST_DECK = new Deck( 3L, 3, 30L, 4L, 30L);

    private static final List<Deck> TEST_DECKS = Arrays.asList(
            TEST_FIRST_DECK,
            TEST_CURRENT_DECK,
            TEST_LAST_DECK
    );

    private static final Card TEST_FIRST_CARD = new Card(1L, "Dog", "Собака", 8L, TEST_FIRST_DECK.getDeckId(), 1L);
    private static final Card TEST_SECOND_CARD = new Card(2L, "Cat", "Кошка", 4L, TEST_FIRST_DECK.getDeckId(), 2L);
    private static final Card TEST_CARD_IN_MIDDLE_DECK = new Card(3L, "Bird", "Птица", 10L, TEST_CURRENT_DECK.getDeckId(), 3L);

    private static final List<Card> TEST_CARDS = new ArrayList<>(Arrays.asList(
            TEST_FIRST_CARD,
            TEST_SECOND_CARD,
            TEST_CARD_IN_MIDDLE_DECK
    ));

    private static final SessionLog TEST_FIRST_SESSION_LOG = new SessionLog(1L, 1L,2L,3,4,5, 2.5);
    private static final SessionLog TEST_SECOND_SESSION_LOG = new SessionLog(2L, 3L,4L,4,5,6,3.5);

    private static final List<SessionLog> TEST_SESSION_LOGS = Arrays.asList(
            TEST_FIRST_SESSION_LOG,
            TEST_SECOND_SESSION_LOG
    );

    private static final ReviewLog TEST_FIRST_REVIEW_LOG = new ReviewLog(1L, 2L, TEST_FIRST_DECK.getDeckId(), 4L);
    private static final ReviewLog TEST_SECOND_REVIEW_LOG = new ReviewLog(2L, 3L, TEST_FIRST_DECK.getDeckId(), 5L);

    private static final List<ReviewLog> TEST_REVIEW_LOGS = Arrays.asList(
            TEST_FIRST_REVIEW_LOG,
            TEST_SECOND_REVIEW_LOG
    );

    public static List<Deck> getTestDecks() {
        return TEST_DECKS;
    }

    public static List<Card> getTestCards() {
        return TEST_CARDS;
    }

    public static Deck getTestFirstDeck() {
        return TEST_FIRST_DECK;
    }

    public static Deck getTestCurrentDeck() {
        return TEST_CURRENT_DECK;
    }

    public static Deck getTestLastDeck() {
        return TEST_LAST_DECK;
    }

    public static Card getTestFirstCard() {
        return TEST_FIRST_CARD;
    }

    public static Card getTestSecondCard() {
        return TEST_SECOND_CARD;
    }

    public static Card getTestCardInMiddleDeck() {
        return TEST_CARD_IN_MIDDLE_DECK;
    }

    public static SessionLog getTestFirstSessionLog() {
        return TEST_FIRST_SESSION_LOG;
    }

    public static SessionLog getTestSecondSessionLog() {
        return TEST_SECOND_SESSION_LOG;
    }

    public static List<SessionLog> getTestSessionLogs() {
        return TEST_SESSION_LOGS;
    }

    public static ReviewLog getTestFirstReviewLog() {
        return TEST_FIRST_REVIEW_LOG;
    }

    public static ReviewLog getTestSecondReviewLog() {
        return TEST_SECOND_REVIEW_LOG;
    }

    public static List<ReviewLog> getTestReviewLogs() {
        return TEST_REVIEW_LOGS;
    }
}

package daniil.shevtsov.com.spacedrecall.service.repository;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import daniil.shevtsov.com.spacedrecall.api.retrofit.RestApiInterface;
import daniil.shevtsov.com.spacedrecall.api.retrofit.model.OptimizationProblemResult;
import daniil.shevtsov.com.spacedrecall.service.model.schedule.OptimizationInputData;
import daniil.shevtsov.com.spacedrecall.util.Logger;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

import static org.mockito.Mockito.doReturn;

public class OptimalServiceRatesSolverTest {
    private static final double ARRIVAL_RATE = 0.5;
    private static final List<Double> INJECTION_RATES = Arrays.asList(1.0, 2.0);
    private static final double REVIEW_FREQUENCY_BUDGET = 1.2;

    private static final List<Double> SERVICE_RATES = Arrays.asList(0.5, 0.8);

    private static final OptimizationInputData OPTIMIZATION_INPUT_DATA =
            new OptimizationInputData(ARRIVAL_RATE, INJECTION_RATES, REVIEW_FREQUENCY_BUDGET);

    private OptimalServiceRatesSolver mOptimalServiceRatesSolver;

    @Mock
    RestApiInterface mRestApiInterface;

    @Mock
    Logger mLogger;

    @Before
    public void onSetup() {
        MockitoAnnotations.initMocks(this);

        mOptimalServiceRatesSolver = new OptimalServiceRatesSolver(mRestApiInterface, mLogger);
    }

    @Test
    public void testCalculateOptimalServiceRates() {
        doReturn(Single.just(new OptimizationProblemResult(ARRIVAL_RATE, SERVICE_RATES)))
                .when(mRestApiInterface).calculateOptimalServiceRates(ARRIVAL_RATE,
                INJECTION_RATES, REVIEW_FREQUENCY_BUDGET);

        mOptimalServiceRatesSolver.calculateOptimalServiceRates(OPTIMIZATION_INPUT_DATA)
                .subscribeOn(Schedulers.trampoline())
                .test()
                .awaitDone(5, TimeUnit.SECONDS)
                .assertValue(SERVICE_RATES);
    }
}

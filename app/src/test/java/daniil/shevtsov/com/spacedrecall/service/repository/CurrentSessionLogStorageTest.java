package daniil.shevtsov.com.spacedrecall.service.repository;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class CurrentSessionLogStorageTest {
    private static final long SESSION_START_TIMESTAMP = 2L;
    private static final long SESSION_END_TIMESTAMP = 6L;

    private CurrentSessionLogStorage mCurrentSessionLogStorage;

    @Before
    public void onSetup() {
        mCurrentSessionLogStorage = new CurrentSessionLogStorage();
    }

    //TODO: a lot of testing implementation details going on here

    @Test
    public void testSetSessionStartTimestamp() {
        mCurrentSessionLogStorage.setSessionStartedTimestamp(SESSION_START_TIMESTAMP);

        assertThat(mCurrentSessionLogStorage.getSessionStartTimestamp(), is(SESSION_START_TIMESTAMP));
    }

    @Test
    public void testSetSessionEndTimestamp() {
        mCurrentSessionLogStorage.setSessionFinishedTimestamp(SESSION_END_TIMESTAMP);

        assertThat(mCurrentSessionLogStorage.getSessionFinishTimestamp(), is(SESSION_END_TIMESTAMP));
    }

    @Test
    public void testIncreaseAddedCardsCount() {
        int oldCount = mCurrentSessionLogStorage.getAddedCardsCount();
        mCurrentSessionLogStorage.increaseAddedCardsCount();
        assertThat(mCurrentSessionLogStorage.getAddedCardsCount(), is(++oldCount));
    }

    @Test
    public void testIncreaseReviewedCardsCount() {
        int oldCount = mCurrentSessionLogStorage.getReviewedCardsCount();
        mCurrentSessionLogStorage.increaseReviewedCardsCount();
        assertThat(mCurrentSessionLogStorage.getReviewedCardsCount(), is(++oldCount));
    }

    @Test
    public void testIncreaseMemorizedCardsCount() {
        int oldCount = mCurrentSessionLogStorage.getMemorizedCardsCount();
        mCurrentSessionLogStorage.increaseMemorizedCardsCount();
        assertThat(mCurrentSessionLogStorage.getMemorizedCardsCount(), is(++oldCount));
    }

    @Test
    public void testSetReviewFrequencyBudget() {
        int reviewCount = 2;
        long sessionLength = SESSION_END_TIMESTAMP - SESSION_START_TIMESTAMP;
        double reviewFrequencyBudget = (double) reviewCount / sessionLength;

        mCurrentSessionLogStorage.setSessionStartedTimestamp(SESSION_START_TIMESTAMP);
        mCurrentSessionLogStorage.setSessionFinishedTimestamp(SESSION_END_TIMESTAMP);
        mCurrentSessionLogStorage.increaseReviewedCardsCount();
        mCurrentSessionLogStorage.increaseReviewedCardsCount();

        mCurrentSessionLogStorage.setReviewFrequencyBudget();

        assertThat(mCurrentSessionLogStorage.getReviewFrequencyBudget(), is(reviewFrequencyBudget));
    }
}

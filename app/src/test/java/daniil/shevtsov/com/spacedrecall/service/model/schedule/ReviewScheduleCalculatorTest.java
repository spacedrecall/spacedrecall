package daniil.shevtsov.com.spacedrecall.service.model.schedule;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

import daniil.shevtsov.com.spacedrecall.api.room.model.entity.Card;
import daniil.shevtsov.com.spacedrecall.service.model.statistics.StatisticsCalculator;
import daniil.shevtsov.com.spacedrecall.service.repository.OptimalServiceRatesSolver;
import daniil.shevtsov.com.spacedrecall.service.repository.TimestampProvider;
import daniil.shevtsov.com.spacedrecall.service.repository.database.CardProvider;
import daniil.shevtsov.com.spacedrecall.service.repository.database.DeckProvider;
import daniil.shevtsov.com.spacedrecall.util.Logger;
import daniil.shevtsov.com.spacedrecall.util.TestDataProvider;
import io.reactivex.Completable;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

public class ReviewScheduleCalculatorTest {
    private static final Card TEST_FIRST_CARD = TestDataProvider.getTestFirstCard();

    private static final double ARRIVAL_RATE = 0.5;
    private static final List<Double> INJECTION_RATES = Arrays.asList(0.1, 0.2, 0.3, 0.4);
    private static final double FREQUENCY_REVIEW_BUDGET = 1.2;
    private static final OptimizationInputData OPTIMIZATION_INPUT_DATA = new OptimizationInputData(ARRIVAL_RATE, INJECTION_RATES, FREQUENCY_REVIEW_BUDGET);

    private static final List<Double> OPTIMAL_SERVICE_RATES = Arrays.asList(0.5, 1.0, 1.5, 2.0);
    private static final List<Long> REPETITION_INTERVALS = Arrays.asList(4L, 8L, 15L, 16L);

    private static final long CURRENT_TIMESTAMP = 10L;

    private ReviewScheduleCalculator mReviewScheduleCalculator;

    @Mock
    private OptimalServiceRatesSolver mOptimalServiceRatesSolver;

    @Mock
    private StatisticsCalculator mStatisticsCalculator;

    @Mock
    private CardProvider mCardProvider;

    @Mock
    private DeckProvider mDeckProvider;

    @Mock
    private TimestampProvider mTimestampProvider;

    @Mock
    private Logger mLogger;

    private final Scheduler mTestScheduler = Schedulers.trampoline();

    @Before
    public void onSetup() {
        MockitoAnnotations.initMocks(this);

        doReturn(Single.just(OPTIMIZATION_INPUT_DATA)).when(mStatisticsCalculator).calculateOptimizationInputData();
        doReturn(REPETITION_INTERVALS).when(mStatisticsCalculator).calculateRepetitionIntervals(OPTIMAL_SERVICE_RATES);

        doReturn(Completable.complete()).when(mDeckProvider).updateRepetitionIntervals(REPETITION_INTERVALS);

        doReturn(Single.just(OPTIMAL_SERVICE_RATES))
                .when(mOptimalServiceRatesSolver)
                .calculateOptimalServiceRates(OPTIMIZATION_INPUT_DATA);

        doReturn(CURRENT_TIMESTAMP).when(mTimestampProvider).getCurrentTimestamp();

        mReviewScheduleCalculator = new ReviewScheduleCalculator(mOptimalServiceRatesSolver,
                mStatisticsCalculator, mDeckProvider, mLogger);
    }

    @Test
    public void testUpdateServiceRates() {
        mReviewScheduleCalculator.updateServiceRates()
                .subscribeOn(mTestScheduler)
                .blockingAwait();

        verify(mDeckProvider).updateRepetitionIntervals(REPETITION_INTERVALS);

    }
}

package daniil.shevtsov.com.spacedrecall.viewmodels;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.Observer;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import daniil.shevtsov.com.spacedrecall.service.model.schedule.NextDeckCalculator;
import daniil.shevtsov.com.spacedrecall.service.model.statistics.SessionLogger;
import daniil.shevtsov.com.spacedrecall.service.repository.database.DeckProvider;
import daniil.shevtsov.com.spacedrecall.util.Logger;
import daniil.shevtsov.com.spacedrecall.viewmodel.MainViewModel;
import io.reactivex.Completable;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.internal.verification.VerificationModeFactory.times;

public class MainViewModelTest {
    private static final int NUMBER_OF_DECKS = NextDeckCalculator.MAX_DECK_NUMBER;

    @Rule
    public TestRule rule = new InstantTaskExecutorRule();

    private MainViewModel mMainViewModel;

    @Mock
    private Observer<Void> mEventObserver;

    @Mock
    private Logger mLogger;

    @Mock
    private SessionLogger mSessionLogger;

    @Mock
    private DeckProvider mDeckProvider;

    private Scheduler mTestScheduler;

    @Before
    public void onSetup() {
        MockitoAnnotations.initMocks(this);

        mTestScheduler = Schedulers.trampoline();

        doReturn(Single.just(false)).when(mDeckProvider).checkIfDecksExist();
        doReturn(Completable.complete()).when(mDeckProvider).createDecks(NUMBER_OF_DECKS);

        mMainViewModel = new MainViewModel(mLogger, mSessionLogger, mDeckProvider,
                mTestScheduler, mTestScheduler);
    }

    @Test
    public void testOnAddCards() {
        mMainViewModel.getNavigateToAddCardsEvent().observeForever(mEventObserver);

        mMainViewModel.onAddCards();

        verify(mEventObserver, times(1))
                .onChanged(null);
    }

    @Test
    public void testOnStudy() {
        mMainViewModel.getNavigateToStudyEvent().observeForever(mEventObserver);

        mMainViewModel.onStudyCards();

        verify(mEventObserver, times(1))
                .onChanged(null);
    }

    @Test
    public void testInitialiseDecks() {
        mMainViewModel.initialiseDecks();

        verify(mDeckProvider).createDecks(NUMBER_OF_DECKS);
    }
}
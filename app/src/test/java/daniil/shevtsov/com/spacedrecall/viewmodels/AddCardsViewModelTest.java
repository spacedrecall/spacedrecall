package daniil.shevtsov.com.spacedrecall.viewmodels;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.Observer;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import daniil.shevtsov.com.spacedrecall.api.room.model.entity.Card;
import daniil.shevtsov.com.spacedrecall.service.model.statistics.SessionLogger;
import daniil.shevtsov.com.spacedrecall.service.repository.TimestampProvider;
import daniil.shevtsov.com.spacedrecall.service.repository.database.CardProvider;
import daniil.shevtsov.com.spacedrecall.service.repository.database.DeckProvider;
import daniil.shevtsov.com.spacedrecall.util.Logger;
import daniil.shevtsov.com.spacedrecall.viewmodel.AddCardsViewModel;
import io.reactivex.Completable;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

public class AddCardsViewModelTest {
    private static final String TEST_QUESTION = "dog";
    private static final String TEST_ANSWER = "собака";

    private static final long TEST_DECK_ID = 1L;

    @Rule
    public TestRule rule = new InstantTaskExecutorRule();

    @Mock
    private Logger mLogger;

    @Mock
    private CardProvider mCardProvider;

    @Mock
    private DeckProvider mDeckProvider;

    @Mock
    private SessionLogger mSessionLogger;

    @Mock
    private TimestampProvider mTimestampProvider;

    private Scheduler mTestScheduler;

    private AddCardsViewModel mAddCardsViewModel;

    @Mock
    private Observer<Void> mCardInsertedEventObserver;

    @Mock
    private Observer<Boolean> mIsCardValidObserver;

    @Captor
    private ArgumentCaptor<Card> mNewCardArgumentCaptor;

    @Before
    public void onSetup() {
        MockitoAnnotations.initMocks(this);

        mTestScheduler = Schedulers.trampoline();

        doReturn(Single.just(TEST_DECK_ID)).when(mDeckProvider).getFirstDeckId();

        mAddCardsViewModel = new AddCardsViewModel(mCardProvider, mDeckProvider,
                mSessionLogger, mLogger, mTimestampProvider, mTestScheduler, mTestScheduler);

        mAddCardsViewModel.getIsCardValidObservable().observeForever(mIsCardValidObserver);
        mAddCardsViewModel.getCardInsertedEvent().observeForever(mCardInsertedEventObserver);
    }

    @Test
    public void testSessionLoggerOnAddCardCalled() {
        addCard();

        verify(mSessionLogger).onCardAdded();
    }

    private void addCard() {
        doReturn(Completable.complete()).when(mCardProvider).addNewCard(any(Card.class));

        mAddCardsViewModel.getQuestionTextObservable().postValue(TEST_QUESTION);
        mAddCardsViewModel.getAnswerTextObservable().postValue(TEST_ANSWER);

        mAddCardsViewModel.onAddCard();
    }

    @Test
    public void testInsertingCardMethodCalled() {
        addCard();

        verify(mCardProvider).addNewCard(mNewCardArgumentCaptor.capture());

        assertThat(mNewCardArgumentCaptor.getValue().getQuestion(), is(TEST_QUESTION));

        verify(mCardInsertedEventObserver).onChanged(nullable(Void.class));
    }

    @Test
    public void testInvalidQuestion() {
        mAddCardsViewModel.onQuestionTextChange("");
        mAddCardsViewModel.onAnswerTextChange(TEST_ANSWER);

        verify(mIsCardValidObserver, atLeastOnce()).onChanged(false);
    }

    @Test
    public void testInvalidAnswer() {
        mAddCardsViewModel.onQuestionTextChange(TEST_QUESTION);
        mAddCardsViewModel.onAnswerTextChange("");

        verify(mIsCardValidObserver, atLeastOnce()).onChanged(false);
    }

    @Test
    public void testInvalidQuestionAndAnswer() {
        mAddCardsViewModel.onQuestionTextChange("");
        mAddCardsViewModel.onAnswerTextChange("");

        verify(mIsCardValidObserver, atLeastOnce()).onChanged(false);
    }

    @Test
    public void testValidQuestionAndAnswer() {
        mAddCardsViewModel.onQuestionTextChange(TEST_QUESTION);
        mAddCardsViewModel.onAnswerTextChange(TEST_ANSWER);

        verify(mIsCardValidObserver).onChanged(true);
    }

    @Test
    public void testQuestionNull() {
        mAddCardsViewModel.onQuestionTextChange(null);
        mAddCardsViewModel.onAnswerTextChange(TEST_ANSWER);

        mAddCardsViewModel.onAddCard();

        verify(mLogger).e(anyString(), anyString());
        verify(mCardProvider, never()).addNewCard(any(Card.class));
    }

    @Test
    public void testAnswerNull() {
        mAddCardsViewModel.onQuestionTextChange(TEST_QUESTION);
        mAddCardsViewModel.onAnswerTextChange(null);

        mAddCardsViewModel.onAddCard();

        verify(mLogger).e(anyString(), anyString());
        verify(mCardProvider, never()).addNewCard(any(Card.class));
    }

    @Test
    public void testAddCardException() {
        mAddCardsViewModel.onQuestionTextChange(TEST_QUESTION);
        mAddCardsViewModel.onAnswerTextChange(TEST_ANSWER);

        doReturn(Single.error(new Throwable())).when(mDeckProvider).getFirstDeckId();
        mAddCardsViewModel.onAddCard();

        verify(mLogger).e(anyString(), anyString());
        verify(mCardProvider, never()).addNewCard(any(Card.class));
    }
}

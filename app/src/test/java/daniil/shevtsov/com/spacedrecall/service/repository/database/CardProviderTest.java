package daniil.shevtsov.com.spacedrecall.service.repository.database;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import daniil.shevtsov.com.spacedrecall.api.room.model.dao.CardDao;
import daniil.shevtsov.com.spacedrecall.api.room.model.entity.Card;
import daniil.shevtsov.com.spacedrecall.api.room.model.entity.Deck;
import daniil.shevtsov.com.spacedrecall.util.TestDataProvider;
import io.reactivex.Single;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

public class CardProviderTest {
    private static final Card TEST_FIRST_CARD = TestDataProvider.getTestFirstCard();
    private static final Deck TEST_FIRST_DECK = TestDataProvider.getTestFirstDeck();

    private static final List<Card> TEST_CARDS_TO_REPEAT = TestDataProvider.getTestCards();

    private static final long TEST_NEW_DECK_ID = 2;

    private static final long TEST_NEW_LAST_REPETITION_TIME = 4;

    private static final long TEST_CURRENT_TIMESTAMP = 10;

    private CardProvider mCardProvider;

    @Mock
    private CardDao mCardDao;

    @Before
    public void onSetup() {
        MockitoAnnotations.initMocks(this);

        mCardProvider = new CardProvider(mCardDao);
    }

    @Test
    public void testGetEarliestCardFromDeck() {
        doReturn(Single.just(TEST_FIRST_CARD)).when(mCardDao).selectEarliestCardFromDeck(TEST_FIRST_DECK.getDeckId());

        mCardProvider.getEarliestCardFromDeck(TEST_FIRST_DECK.getDeckId())
                .test()
                .assertValue(TEST_FIRST_CARD);
    }

    @Test
    public void testMoveCardToAnotherDeck() {
        mCardProvider.moveCardToAnotherDeck(TEST_FIRST_CARD, TEST_NEW_DECK_ID);

        verify(mCardDao).updateCardDeckId(TEST_FIRST_CARD.getCardId(), TEST_NEW_DECK_ID);
    }

    @Test
    public void testUpdateLastRepetitionTime() {
        mCardProvider.updateLastRepetitionTime(TEST_FIRST_CARD.getCardId(), TEST_NEW_LAST_REPETITION_TIME);

        verify(mCardDao).updateCardLastRepetitionTimestamp(TEST_FIRST_CARD.getCardId(), TEST_NEW_LAST_REPETITION_TIME);
    }

    @Test
    public void testAddNewCard() {
        doReturn(Single.just(TEST_FIRST_CARD.getCardId()))
                .when(mCardDao).insert(TEST_FIRST_CARD);

        mCardProvider.addNewCard(TEST_FIRST_CARD)
        .test()
        .onComplete();

        verify(mCardDao).insert(TEST_FIRST_CARD);
    }

    @Test
    public void testCountCardsAvailableForRepetition() {
        int cardsCount = 5;

        doReturn(Single.just(cardsCount)).when(mCardDao).countCardsAvailableForRepetition(TEST_FIRST_DECK.getDeckId());

        mCardProvider.countCardsAvailableForRepetition(TEST_FIRST_DECK.getDeckId())
                .test()
                .assertValue(cardsCount);
    }
}

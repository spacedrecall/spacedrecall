package daniil.shevtsov.com.spacedrecall;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Calendar;

import daniil.shevtsov.com.spacedrecall.service.model.datetime.Clock;
import daniil.shevtsov.com.spacedrecall.service.model.datetime.DateWorker;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

public class DateWorkerTest {
    private DateWorker mDateWorker;

    private static final int DAYS_INCREMENT = 1;

    @Mock
    private Clock mClock;

    @Mock
    private Calendar mCalendar;

    @Before
    public void onSetup() {
        MockitoAnnotations.initMocks(this);

        doReturn(mCalendar).when(mClock).getCalendarInstance();

        mDateWorker = new DateWorker(mClock);
    }

    @After
    public void onTearDown() {

    }

    @Test
    public void testGetCurrentDate() {
        mDateWorker.getCurrentDate();

        verify(mCalendar).getTime();

    }

    @Test
    public void testAddDays() {
        mDateWorker.addDays(DAYS_INCREMENT);

        verify(mCalendar).add(eq(Calendar.DATE), eq(DAYS_INCREMENT));
    }
}

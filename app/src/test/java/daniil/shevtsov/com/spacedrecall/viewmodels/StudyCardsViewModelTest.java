package daniil.shevtsov.com.spacedrecall.viewmodels;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.Observer;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import daniil.shevtsov.com.spacedrecall.api.room.model.entity.Card;
import daniil.shevtsov.com.spacedrecall.api.room.model.entity.Deck;
import daniil.shevtsov.com.spacedrecall.service.model.schedule.CardScheduler;
import daniil.shevtsov.com.spacedrecall.service.model.statistics.ReviewLogger;
import daniil.shevtsov.com.spacedrecall.service.model.statistics.SessionLogger;
import daniil.shevtsov.com.spacedrecall.util.Logger;
import daniil.shevtsov.com.spacedrecall.util.TestDataProvider;
import daniil.shevtsov.com.spacedrecall.viewmodel.StudyCardsViewModel;
import io.reactivex.Completable;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class StudyCardsViewModelTest {
    private static final Card TEST_FIRST_CARD = TestDataProvider.getTestFirstCard();
    private static final Card TEST_SECOND_CARD = TestDataProvider.getTestSecondCard();

    private static final Deck TEST_FIRST_DECK = TestDataProvider.getTestFirstDeck();

    @Rule
    public TestRule rule = new InstantTaskExecutorRule();

    private StudyCardsViewModel mStudyCardsViewModel;

    @Mock
    private CardScheduler mCardScheduler;

    @Mock
    private Logger mLogger;

    @Mock
    private ReviewLogger mReviewLogger;

    @Mock
    private SessionLogger mSessionLogger;

    @Mock
    private Observer<String> mQuestionTextObserver;

    @Mock
    private Observer<String> mAnswerTextObserver;

    @Mock
    private Observer<Boolean> mIsShowAnswerButtonVisibleObserver;

    @Mock
    private Observer<Boolean> mIsRecallButtonsVisibleObserver;

    @Mock
    private Observer<Void> mShowNextCardEventObserver;

    @Mock
    private Observer<Void> mCardsFinishedEventObserver;

    private Scheduler mTestScheduler;

    @Mock
    private Observer<Void> mShowAnswerEventObserver;

    @Before
    public void onSetup() {
        MockitoAnnotations.initMocks(this);

        mTestScheduler = Schedulers.trampoline();

        doReturn(Single.just(TEST_FIRST_CARD), Single.just(TEST_SECOND_CARD))
                .when(mCardScheduler).getNextCardToReview();

        doReturn(Single.just(true)).when(mCardScheduler).checkIfMoreCardsToRepeatAvailable();

        mStudyCardsViewModel = new StudyCardsViewModel(mCardScheduler, mLogger, mSessionLogger,
                mReviewLogger, mTestScheduler, mTestScheduler);

        mStudyCardsViewModel.getQuestionTextObservable().observeForever(mQuestionTextObserver);
        mStudyCardsViewModel.getAnswerTextObservable().observeForever(mAnswerTextObserver);
        mStudyCardsViewModel.getIsShowAnswerButtonVisibleLiveData().observeForever(mIsShowAnswerButtonVisibleObserver);
        mStudyCardsViewModel.getIsRecallButtonsVisibleLiveData().observeForever(mIsRecallButtonsVisibleObserver);

        mStudyCardsViewModel.getShowNextCardEvent().observeForever(mShowNextCardEventObserver);
        mStudyCardsViewModel.getCardsFinishedEvent().observeForever(mCardsFinishedEventObserver);
        mStudyCardsViewModel.getShowAnswerEvent().observeForever(mShowAnswerEventObserver);
    }

    @Test
    public void testShowFirstCard() {
        mStudyCardsViewModel.onShowNextCard();

        verify(mQuestionTextObserver).onChanged(TEST_FIRST_CARD.getQuestion());
        verify(mAnswerTextObserver).onChanged("");
    }

    @Test
    public void testShowFirstCardAnswer() {
        mStudyCardsViewModel.onShowNextCard();

        mStudyCardsViewModel.onShowAnswer();

        verify(mAnswerTextObserver).onChanged(TEST_FIRST_CARD.getAnswer());
    }

    @Test
    public void testOnShowNextCard() {
        mStudyCardsViewModel.onShowNextCard();
        mStudyCardsViewModel.onShowNextCard();

        verify(mQuestionTextObserver).onChanged(TEST_SECOND_CARD.getQuestion());
        verify(mAnswerTextObserver, times(2)).onChanged("");
    }

    @Test
    public void testShowNextCardAnswer() {
        mStudyCardsViewModel.onShowNextCard();

        mStudyCardsViewModel.onShowNextCard();

        mStudyCardsViewModel.onShowAnswer();

        verify(mAnswerTextObserver).onChanged(TEST_SECOND_CARD.getAnswer());
    }

    @Test
    public void testShowingAnswerEvent() {
        mStudyCardsViewModel.onShowNextCard();

        mStudyCardsViewModel.onShowAnswer();

        verify(mShowAnswerEventObserver).onChanged(nullable(Void.class));
    }

    @Test
    public void testOnCardReviewed() {
        mStudyCardsViewModel.onShowNextCard();

        doReturn(Completable.complete()).when(mCardScheduler).recalculateServiceRates(TEST_FIRST_CARD);
        doReturn(Single.just(TEST_FIRST_DECK.getDeckId())).when(mCardScheduler).moveCardToAnotherDeck(TEST_FIRST_CARD, true);

        mStudyCardsViewModel.onCardReviewed(true);

        verify(mCardScheduler).recalculateServiceRates(TEST_FIRST_CARD);
        verify(mCardScheduler).moveCardToAnotherDeck(TEST_FIRST_CARD, true);

        verify(mShowNextCardEventObserver).onChanged(nullable(Void.class));
    }

    @Test
    public void testShowAnswerButtonVisibleInitially() {
        verify(mIsShowAnswerButtonVisibleObserver).onChanged(true);
        verify(mIsRecallButtonsVisibleObserver).onChanged(false);
    }

    @Test
    public void testRecallButtonVisibleAfterShowingAnswer() {
        mStudyCardsViewModel.onShowNextCard();
        mStudyCardsViewModel.onShowAnswer();

        verify(mIsShowAnswerButtonVisibleObserver).onChanged(false);
        verify(mIsRecallButtonsVisibleObserver).onChanged(true);
    }

    @Test
    public void testShowAnswerButtonVisibleAfterRecall() {
        mStudyCardsViewModel.onShowNextCard();

        doReturn(Completable.complete()).when(mCardScheduler).recalculateServiceRates(TEST_FIRST_CARD);
        doReturn(Single.just(TEST_FIRST_DECK.getDeckId())).when(mCardScheduler).moveCardToAnotherDeck(TEST_FIRST_CARD, true);

        mStudyCardsViewModel.onCardReviewed(true);

        verify(mIsShowAnswerButtonVisibleObserver, times(2)).onChanged(true);
        verify(mIsRecallButtonsVisibleObserver, times(2)).onChanged(false);
    }

    @Test
    public void testOnCardReviewedLogged() {
        mStudyCardsViewModel.onShowNextCard();

        doReturn(Completable.complete()).when(mCardScheduler).recalculateServiceRates(TEST_FIRST_CARD);
        doReturn(Single.just(TEST_FIRST_DECK.getDeckId())).when(mCardScheduler).moveCardToAnotherDeck(TEST_FIRST_CARD, true);

        mStudyCardsViewModel.onCardReviewed(true);

        verify(mSessionLogger).onCardReviewed();
        verify(mReviewLogger).onCardReviewed(TEST_FIRST_DECK.getDeckId());

    }

    @Test
    public void testCardsFinishedEvent() {
        // simulate case when there are no cards to repeat for current date
        doReturn(Single.just(false)).when(mCardScheduler).checkIfMoreCardsToRepeatAvailable();

        mStudyCardsViewModel.onShowNextCard();

        verify(mCardsFinishedEventObserver).onChanged(nullable(Void.class));

        verify(mCardScheduler, never()).getNextCardToReview();
    }
}

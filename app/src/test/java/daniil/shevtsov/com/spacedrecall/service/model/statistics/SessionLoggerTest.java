package daniil.shevtsov.com.spacedrecall.service.model.statistics;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import daniil.shevtsov.com.spacedrecall.api.room.model.entity.SessionLog;
import daniil.shevtsov.com.spacedrecall.service.repository.CurrentSessionLogStorage;
import daniil.shevtsov.com.spacedrecall.service.repository.TimestampProvider;
import daniil.shevtsov.com.spacedrecall.service.repository.database.SessionLogsRepository;
import daniil.shevtsov.com.spacedrecall.util.Logger;
import io.reactivex.Completable;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

public class SessionLoggerTest {
    private static final long CURRENT_TIMESTAMP = 5L;

    private static final SessionLog SESSION_LOG = new SessionLog(1L, 1L,
            2L, 3, 4, 5,6.2);

    @Mock
    private Logger mLogger;

    @Mock
    private ReviewLogger mReviewLogger;

    @Mock
    private TimestampProvider mTimestampProvider;

    @Mock
    private SessionLogsRepository mSessionLogsRepository;

    @Mock
    private CurrentSessionLogStorage mCurrentSessionLogStorage;

    private Scheduler mTestScheduler;

    private SessionLogger mSessionLogger;

    @Before
    public void onSetup() {
        MockitoAnnotations.initMocks(this);

        mTestScheduler = Schedulers.trampoline();

        doReturn(CURRENT_TIMESTAMP).when(mTimestampProvider).getCurrentTimestamp();

        mSessionLogger = new SessionLogger(mLogger, mReviewLogger, mTimestampProvider,
                mSessionLogsRepository, mCurrentSessionLogStorage, mTestScheduler, mTestScheduler);
    }

    @Test
    public void testOnSessionStarted() {
        mSessionLogger.onSessionStarted();

        verify(mCurrentSessionLogStorage).setSessionStartedTimestamp(CURRENT_TIMESTAMP);
    }

    @Test
    public void testOnSessionFinished() {
        doReturn(Single.just(SESSION_LOG.getSessionLogId())).when(mSessionLogsRepository).addNewSessionLog(SESSION_LOG);
        doReturn(SESSION_LOG).when(mCurrentSessionLogStorage).getCurrentSessionLog();
        doReturn(Completable.complete()).when(mReviewLogger).onSessionFinished(SESSION_LOG.getSessionLogId());

        mSessionLogger.onSessionFinished();

        verify(mCurrentSessionLogStorage).setReviewFrequencyBudget();

        verify(mCurrentSessionLogStorage).setSessionFinishedTimestamp(CURRENT_TIMESTAMP);

        verify(mSessionLogsRepository).addNewSessionLog(SESSION_LOG);
    }

    @Test
    public void testOnCardAdded() {
        mSessionLogger.onCardAdded();

        verify(mCurrentSessionLogStorage).increaseAddedCardsCount();
    }

    @Test
    public void testOnCardReviewed() {
        mSessionLogger.onCardReviewed();

        verify(mCurrentSessionLogStorage).increaseReviewedCardsCount();
    }

    @Test
    public void testOnCardMemorized() {
        mSessionLogger.onCardMemorized();

        verify(mCurrentSessionLogStorage).increaseMemorizedCardsCount();
    }

}
